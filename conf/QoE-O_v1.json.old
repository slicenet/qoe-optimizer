{
  "openapi": "3.0.0",
  "info": {
    "description": "This is the API exposed by the QoE Optimizer",
    "version": "1.0.0",
    "title": "qoe_optimizer_v1",
    "contact": {
      "email": "agraz@tsc.upc.edu"
    },
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    }
  },
  "servers": [
    {
      "url": "http://localhost:65105/"
    }
  ],
  "paths": {
    "/conf": {
      "post": {
        "summary": "Configure the parameters of the Slice",
        "description": "The operation is used to configure a set of parameters coming from the Management",
        "operationId": "confPlugin",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/QoEConf"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "OK"
          },
          "400": {
            "description": "Bad Request"
          }
        }
      }
    },
    "/qoe": {
      "put": {
        "summary": "Set the perceived QoE",
        "description": "The operation is used to request an increase of the QoE if needed",
        "operationId": "send-QoE-Feedback",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/QoEObject"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "OK"
          },
          "400": {
            "description": "Bad Request"
          }
        }
      }
    },
    "/qoe/{id}": {
      "get": {
        "summary": "Get the perceived QoE",
        "description": "The operation is used to request the last perceived QoE",
        "operationId": "get-Last-QoE-Feedback",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "Id of the QoE Instance",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                },
                "example": "GOOD"
              }
            }
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "QoEObject": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string"
          },
          "value": {
            "type": "string"
          }
        }
      },
      "QoEConf": {
        "type": "object",
        "properties": {
          "slice_id": {
            "type": "string"
          },
          "slice_owner": {
            "type": "string"
          },
          "register_ip": {
            "type": "string"
          },
          "register_port": {
            "type": "string"
          }
        }
      }
    }
  }
}
