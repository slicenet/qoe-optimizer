package edu.upc.gco.slcnt.qoe.optimizer.core;

import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.CPSType;
import edu.upc.gco.slcnt.qoe.optimizer.cp.CPSR;
import edu.upc.gco.slcnt.qoe.optimizer.orch.Orchestrator;
import edu.upc.gco.slcnt.qoe.optimizer.orch.ServiceOrchestratorDriver;

public class Actuator {

	private Optimizer opt;
	//private QoSControl qos;
	private CPSR cpsr;
	private Orchestrator orchestrator;
	private ServiceOrchestratorDriver soDriver;
	
	public Actuator ()
	{
		
	}
	
	public Actuator (Optimizer opt)
	{
		this.opt = opt;
		//this.qos = new QoSControl(this.opt.getQoe().getCpsr());
		this.cpsr = this.opt.getQoe().getCpsr();
	}
	
	// Get the QoS-C function for the Slice from the CPSR
	/*public String getControlPlaneFunctionURI (CPSType cpsType)
	{
		String sliceId = opt.getQoe().getQoeConf().getSliceId();
		if (this.cpsr != null)
			return this.cpsr.requestFunction(sliceId, cpsType.getValue());
		else
			return sliceId + "/" + cpsType.getValue();
	}*/
	
	public Actuator release()
	{
		this.cpsr = null;
		this.orchestrator = null;
		this.soDriver = null;
		
		return null;
	}

	public Optimizer getOpt() {
		return opt;
	}

	public void setOpt(Optimizer opt) {
		this.opt = opt;
	}

	public CPSR getCpsr() {
		return cpsr;
	}

	public void setCpsr(CPSR cpsr) {
		this.cpsr = cpsr;
	}

	public Orchestrator getOrchestrator() {
		return orchestrator;
	}

	public void setOrchestrator(Orchestrator orchestrator) {
		this.orchestrator = orchestrator;
	}

	public ServiceOrchestratorDriver getSoDriver() {
		return soDriver;
	}

	public void setSoDriver(ServiceOrchestratorDriver soDriver) {
		this.soDriver = soDriver;
	}
}
