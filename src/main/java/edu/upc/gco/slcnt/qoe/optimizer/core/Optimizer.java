package edu.upc.gco.slcnt.qoe.optimizer.core;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upc.gco.slcnt.influxdb.client.datalake.DataLakeApi;
import edu.upc.gco.slcnt.influxdb.client.datalake.model.NNActionPoint;
import edu.upc.gco.slcnt.influxdb.client.datalake.model.NNEventPoint;
import edu.upc.gco.slcnt.pf.client.PFApi;
import edu.upc.gco.slcnt.pf.client.rest.onap.model.Policy;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.ActionOption;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.BootstrapItem;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.Configuration;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.Controller;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.Diagnosis;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.Level;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.TAL;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.TALRule;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.Tactic;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.Threshold;
import edu.upc.gco.slcnt.pf.client.websocket.NotificationListener;
import edu.upc.gco.slcnt.pf.client.websocket.model.Notification;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.ECAPolicy;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.PolicyFactory;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.action.Action;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.action.ActionClass;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.action.ActionType;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.condition.Condition;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.condition.ConditionType;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.event.Event;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.event.EventType;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.monitoring.Sensor;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.monitoring.SensorClass;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.monitoring.SensorDL;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.monitoring.SensorKafka;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.monitoring.SensorType;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.Operator;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.Parameter;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.Value;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueInt;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueString;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueType;
import edu.upc.gco.slcnt.qoe.optimizer.cp.SegmentType;
import edu.upc.gco.slcnt.qoe.optimizer.orch.Orchestrator;
import edu.upc.gco.slcnt.qoe.optimizer.orch.ServiceOrchestratorDriver;
import edu.upc.gco.slcnt.qoe.optimizer.rest.QoE;

class PFListener extends NotificationListener
{
	private Optimizer optimizer;
	
	public PFListener (Optimizer optimizer)
	{
		this.optimizer = optimizer;
	}
	
	@Override
	public void notificationUpdate(Notification notification) {
		this.optimizer.getLogger().info("Received Callback!! = " + notification.toString());
		
		switch (notification.getType())
		{
			case "UPDATE":
				for (edu.upc.gco.slcnt.pf.client.websocket.model.Policy p : notification.getLoadedPolicies())
				{
					if (p.getUpdateType().equals("NEW"))
						optimizer.addPolicy(p.getPolicyName(), p.getVersionNo());
					else if (p.getUpdateType().equals("UPDATE"))
						optimizer.updatePolicy(p.getPolicyName(), p.getVersionNo());
				}
				break;
			case "REMOVE":
				for (edu.upc.gco.slcnt.pf.client.websocket.model.Policy p : notification.getRemovedPolicies())
				{
					optimizer.removePolicy(p.getPolicyName(), p.getVersionNo());
				}
				break;
			case "BOTH":
				for (edu.upc.gco.slcnt.pf.client.websocket.model.Policy p : notification.getRemovedPolicies())
				{
					optimizer.removePolicy(p.getPolicyName(), p.getVersionNo());
				}
				for (edu.upc.gco.slcnt.pf.client.websocket.model.Policy p : notification.getLoadedPolicies())
				{
					if (p.getUpdateType().equals("NEW"))
						optimizer.addPolicy(p.getPolicyName(), p.getVersionNo());
					else if (p.getUpdateType().equals("UPDATE"))
						optimizer.updatePolicy(p.getPolicyName(), p.getVersionNo());
				}
				break;
		}
	}
}

public class Optimizer {
	
	private Log logger = LogFactory.getLog(Optimizer.class);
	private QoE qoe;
	private UUID identifier;
	private HashMap<String,ECAPolicy> policies;
	private Actuator actController;
	private String serverIp;
	private String serverPort;
	private PFApi policyFrameworkClient;
	private PFListener policyFrameworkListener;
	private PolicyFactory policyFactory;
	private DataLakeApi dataLakeClient;
	private String kafkaIp;
	private String kafkaPort;
	
	public Optimizer()
	{
		
	}
	
	public Optimizer (QoE qoe, PFApi pfClient, DataLakeApi dlClient, ServiceOrchestratorDriver soDriver, String kfkIp, String kfkPort)
	{
		this.identifier = UUID.randomUUID();
		this.qoe = qoe;
		this.policies = new HashMap<String,ECAPolicy>();
		this.actController = new Actuator(this);
		
		this.actController.setSoDriver(soDriver);
		
		this.dataLakeClient = dlClient;
		this.dataLakeClient.open();
		
		this.kafkaIp = kfkIp;
		this.kafkaPort = kfkPort;
		
		this.policyFrameworkClient = pfClient;
		this.policyFrameworkListener = new PFListener(this);
		this.policyFrameworkClient.connect(this.policyFrameworkListener);
		List<Policy> pfPolicies = this.policyFrameworkClient.getPolicies();
		this.policyFactory = new PolicyFactory(this);
		this.policyFactory.addPoliciesFromPF(pfPolicies);
		
		pfPolicies.clear();
		pfPolicies = null;
		//Test JSON from OSA (or Policy Framework)
		/*policyFactory = new PolicyFactory(this);
		this.logger.info("Loading JSON");
		TAL tal = null;
		File f = new File("test-policy.json");
		
		try {
			tal = new ObjectMapper().readValue(f, TAL.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		policyFactory.buildPolicyFromTAL(tal);*/
		//
	}
	
	//TODO: Delete uses OpenStack information for Orchestration
	public Optimizer (QoE qoe, String pfProto, String pfIp, String pfPort, String dlIp, String dlPort, String kfkIp, String kfkPort, String ksProto, String ksIp, String ksPort, String ksVersion, String tenantName, String domainName, String userName, String userPwd)
	{
		this.identifier = UUID.randomUUID();
		this.qoe = qoe;
		this.policies = new HashMap<String,ECAPolicy>();
		this.actController = new Actuator(this);
		
		//Orchestrator o = new Orchestrator(ksIp, ksPort, novaIp, novaPort, tenantName, userName, userPwd, adminName, adminPwd);
		//Orchestrator o = new Orchestrator(ksProto, ksIp, ksPort, ksVersion, tenantName, domainName, userName, userPwd);
		//this.actController.setOrchestrator(o);
		//ServiceOrchestratorDriver soDriver = new ServiceOrchestratorDriver (qoe.getQoeObj().getId(), ksProto, ksIp, ksPort);	// Using Keystone parameters for the moment
		ServiceOrchestratorDriver soDriver = new ServiceOrchestratorDriver (qoe.getQoeObj().getId(), /*"http://10.202.11.2:8081"*/"http", "10.202.11.2", "8081","user","user");
		this.actController.setSoDriver(soDriver);
		
		this.dataLakeClient = new DataLakeApi();
		this.dataLakeClient.open("http://" + dlIp + ":" + dlPort);
		
		this.kafkaIp = kfkIp;
		this.kafkaPort = kfkPort;
		
		this.policyFrameworkClient = new PFApi(qoe.getQoeObj().getId(), pfProto, pfIp, pfPort);	// sliceId to filter Policies at Policy Framework
		this.policyFrameworkListener = new PFListener(this);
		this.policyFrameworkClient.connect(this.policyFrameworkListener);
		List<Policy> pfPolicies = this.policyFrameworkClient.getPolicies();
		this.policyFactory = new PolicyFactory(this);
		this.policyFactory.addPoliciesFromPF(pfPolicies);
		
		pfPolicies.clear();
		pfPolicies = null;
		
		//Test JSON from OSA (or Policy Framework)
		/*policyFactory = new PolicyFactory(this);
		this.logger.info("Loading JSON");
		TAL tal = null;
		File f = new File("test-policy.json");
		
		try {
			tal = new ObjectMapper().readValue(f, TAL.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		policyFactory.buildPolicyFromTAL(tal);*/
		//
		
		//this.policyFactory.createPolicies();
	}
	
	public Optimizer (QoE qoe, String pfProto, String pfIp, String pfPort, String dlIp, String dlPort, String kfkIp, String kfkPort, String ssoProto, String ssoIp, String ssoPort, String ssoUser, String ssoPwd)
	{
		this.identifier = UUID.randomUUID();
		this.qoe = qoe;
		this.policies = new HashMap<String,ECAPolicy>();
		this.actController = new Actuator(this);
		
		ServiceOrchestratorDriver soDriver = new ServiceOrchestratorDriver (qoe.getQoeObj().getId(), ssoProto, ssoIp, ssoPort, ssoUser, ssoPwd);
		this.actController.setSoDriver(soDriver);
		
		this.dataLakeClient = new DataLakeApi();
		this.dataLakeClient.open("http://" + dlIp + ":" + dlPort);
		
		this.kafkaIp = kfkIp;
		this.kafkaPort = kfkPort;
		
		this.policyFrameworkClient = new PFApi(qoe.getQoeObj().getId(), pfProto, pfIp, pfPort);	// sliceId to filter Policies at Policy Framework
		this.policyFrameworkListener = new PFListener(this);
		this.policyFrameworkClient.connect(this.policyFrameworkListener);
		List<Policy> pfPolicies = this.policyFrameworkClient.getPolicies();
		this.policyFactory = new PolicyFactory(this);
		this.policyFactory.addPoliciesFromPF(pfPolicies);
		
		pfPolicies.clear();
		pfPolicies = null;
		
		//Test JSON from OSA (or Policy Framework)
		/*policyFactory = new PolicyFactory(this);
		this.logger.info("Loading JSON");
		TAL tal = null;
		File f = new File("test-policy.json");
		
		try {
			tal = new ObjectMapper().readValue(f, TAL.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		policyFactory.buildPolicyFromTAL(tal);*/
		//
		
		//this.policyFactory.createPolicies();
	}
	
	public void updatePolicy (String policyName, String policyVersion)
	{
		logger.info("Updating policy: " + policyName + " (" + policyVersion + ")");
		ECAPolicy oldValue = this.policyFactory.addPolicyFromPFNotification(this.policyFrameworkClient.getPolicy(policyName, policyVersion));
		
		if (oldValue == null)
			logger.warn("No previous version of the policy existed! Not UPDATE");
	}
	
	public void addPolicy (String policyName, String policyVersion)
	{
		logger.info("Adding policy: " + policyName + " (" + policyVersion + ")");
		ECAPolicy oldValue = this.policyFactory.addPolicyFromPFNotification(this.policyFrameworkClient.getPolicy(policyName, policyVersion));
		
		if (oldValue != null)
			logger.warn("Previous version of the policy existed! Not NEW");
	}
	
	public void removePolicy (String policyName, String policyVersion)
	{
		logger.info("Removing policy: " + policyName + " (" + policyVersion + ")");
		ECAPolicy oldValue = this.policies.remove(policyName);
		
		if (oldValue == null)
			logger.warn("The policy didn't exist already! Not DELETE");
		else
		{
			deletePolicy(oldValue);
		}
	}
	
	//TODO: Remove
	public Optimizer (QoE qoe)
	{
		this.identifier = UUID.randomUUID();
		this.qoe = qoe;
		this.policies = new HashMap<String,ECAPolicy>();
		this.actController = new Actuator(this);
		//this.pfClient = new PFApi();
		//this.pfClient.connect();
		this.policyFactory = new PolicyFactory(this);
		
		this.dataLakeClient = new DataLakeApi();
		this.dataLakeClient.open("http://192.168.56.101:8086");
		//this.dlClient.getEvent("noisyneighbordb");
		//Test
		/*String query = "select * from \"event\" where \"tenantid\"='tenantA'";
		NNEventPoint nne = this.dlClient.getLastEvent("noisyneighbordb", query, NNEventPoint.class);
		this.logger.info("Noisy Neighbor Event: " + nne.toString());
		Map<String,Object> fields = new HashMap<String, Object>();
		fields.put("tenantid", nne.getTenantId());
		fields.put("event_ts", nne.getTime().toEpochMilli());
		//this.dlClient.setPoint("noisyneighbordb", "action", null, fields);
		query = "select * from \"action\" where \"tenantid\"='tenantA'";
		NNActionPoint nna = this.dlClient.getLastEvent("noisyneighbordb", query, NNActionPoint.class);
		this.logger.info("Noisy Neighbor Action: " + nna.toString());
		this.logger.info("compareTo == " + nne.getTime().compareTo(nna.getEventTS()));
		if (nne.getTime().getEpochSecond() == nna.getEventTS().getEpochSecond())
			this.logger.info("NNE is related to NNA");*/
		
		this.policyFactory.createPolicies();
	}
	
	private void deletePolicy (ECAPolicy ecaPolicy)
	{
		logger.info("Deleting ECA Policy");
		ecaPolicy.stop();
		ecaPolicy = ecaPolicy.release();
	}
	
	public void stop ()
	{
		Iterator<Entry<String, ECAPolicy>> it = this.policies.entrySet().iterator();
		while (it.hasNext())
		{
			Map.Entry<String, ECAPolicy> entry = (Map.Entry<String, ECAPolicy>) it.next();
			entry.getValue().release();
			it.remove();
		}
		logger.info("Removed all policies from Table (" + this.policies.size() + ")");
		
		this.policyFrameworkClient.disconnect();
		this.dataLakeClient.close();
	}
	
	public Optimizer release ()
	{
		this.policies = null;
		this.policyFrameworkClient = null;
		this.policyFrameworkListener = null;
		this.dataLakeClient = null;
		this.policyFactory = null;
		this.actController = this.actController.release();
		
		return null;
	}
	
	private boolean checkPolicies (Event event)
	{
		for (ECAPolicy p : this.policies.values())
		{
			if (p.getEventType().equals(event.getType()))
				if (p.apply(event.getValue()))
				{
					this.logger.info("Successfully applied Policy: " + p.getName());
					return true;
				}
		}
		
		this.logger.info("No policy found to be applied!");
		return false;
	}
	
	public void generateEvent (EventType type, Value value, String description)
	{
		Event event = new Event (type, value, description);
		//TODO: Save the event to a history list?
		this.checkPolicies(event);
	}
	
	public void generateEvent (Event event)
	{
		this.checkPolicies(event);
	}
	
	public QoE getQoe() {
		return qoe;
	}

	public void setQoe(QoE qoe) {
		this.qoe = qoe;
	}

	public Actuator getActController() {
		return actController;
	}

	public void setActController(Actuator actController) {
		this.actController = actController;
	}

	public UUID getIdentifier() {
		return identifier;
	}

	public void setIdentifier(UUID identifier) {
		this.identifier = identifier;
	}

	public String getServerPort() {
		return serverPort;
	}

	public void setServerPort(String port) {
		this.serverPort = port;
	}

	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

	public DataLakeApi getDataLakeClient() {
		return dataLakeClient;
	}

	public void setDataLakeClient(DataLakeApi dlClient) {
		this.dataLakeClient = dlClient;
	}

	public Log getLogger() {
		return logger;
	}

	public void setLogger(Log logger) {
		this.logger = logger;
	}

	public HashMap<String,ECAPolicy> getPolicies() {
		return policies;
	}

	public PFApi getPolicyFrameworkClient() {
		return policyFrameworkClient;
	}

	public PolicyFactory getPolicyFactory() {
		return policyFactory;
	}

	public String getKafkaIp() {
		return kafkaIp;
	}

	public String getKafkaPort() {
		return kafkaPort;
	}
}
