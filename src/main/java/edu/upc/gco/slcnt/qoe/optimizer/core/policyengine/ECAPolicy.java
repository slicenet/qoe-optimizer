package edu.upc.gco.slcnt.qoe.optimizer.core.policyengine;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.action.Action;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.condition.Condition;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.event.EventType;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.monitoring.Sensor;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.Value;

public class ECAPolicy {

	private Log logger = LogFactory.getLog(ECAPolicy.class);
	private String name;
	private EventType eventType;
	private Condition condition;
	private List<Action> actions;
	private Sensor sensor = null;
	
	public ECAPolicy()
	{
		
	}
	
	public ECAPolicy (String name, EventType type, Condition condition, List<Action> actions)
	{
		this.name = name;
		this.eventType = type;
		this.condition = condition;
		this.actions = actions;
		for (Action a : this.actions)
			a.setPolicy(this);
	}
	
	public ECAPolicy (String name, EventType type, Condition condition, List<Action> actions, Sensor sensor)
	{
		this.name = name;
		this.eventType = type;
		this.condition = condition;
		this.actions = actions;
		this.sensor = sensor;
		for (Action a : this.actions)
			a.setPolicy(this);
	}
	
	public boolean apply (Value input)
	{
		logger.info("Apply = " + this.condition.getParameter().toString() + " / Input=" + input.toString());
		if (this.condition.evaluate(input))
		{
			if (this.actions.isEmpty())
				logger.warn("Action list is empty for Policy: " + name);
			
			for (Action a : this.actions)
			{
				a.actuate();
			}
			return true;
		}
		
		return false;
	}
	
	public void stop ()
	{
		logger.info("Stopping ECAPolicy");
		if (sensor != null)
			sensor.stop();
	}
	
	public ECAPolicy release ()
	{
		logger.info("Releasing ECAPolicy");
		sensor = sensor.release();
		actions.clear();
		logger = null;
		eventType = null;
		condition = null;
		
		return null;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public Condition getCondition() {
		return condition;
	}

	public void setCondition(Condition condition) {
		this.condition = condition;
	}

	public Sensor getSensor() {
		return sensor;
	}

	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}

	public List<Action> getActions() {
		return actions;
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}
}
