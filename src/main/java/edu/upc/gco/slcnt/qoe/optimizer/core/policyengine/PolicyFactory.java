package edu.upc.gco.slcnt.qoe.optimizer.core.policyengine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upc.gco.slcnt.pf.client.rest.onap.model.Policy;
import edu.upc.gco.slcnt.pf.client.rest.onap.model.policy.Config;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.ActionOption;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.Analysis;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.Bootstrap;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.BootstrapItem;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.Configuration;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.Diagnosis;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.Level;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.Output;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.TAL;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.TALRule;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.Tactic;
import edu.upc.gco.slcnt.pf.client.rest.tal.model.Threshold;
import edu.upc.gco.slcnt.qoe.optimizer.core.Optimizer;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.action.Action;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.action.ActionClass;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.action.ActionType;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.condition.Condition;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.condition.ConditionGroup;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.condition.ConditionType;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.event.EventType;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.monitoring.Sensor;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.monitoring.SensorDL;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.monitoring.SensorKafka;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.monitoring.SensorType;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.Operator;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.Parameter;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueInt;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueString;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueType;

public class PolicyFactory {

	private Log logger = LogFactory.getLog(PolicyFactory.class);
	private Optimizer optimizer;
	
	public PolicyFactory (Optimizer optimizer)
	{
		this.optimizer = optimizer;
	}
	
	public void addPoliciesFromPF (List<Policy> pfRestPolicies)
	{
		for (Policy restPolicy : pfRestPolicies)
		{
			ECAPolicy ecaPolicy = getECAPolicyFromPF(restPolicy);
			this.optimizer.getPolicies().put(ecaPolicy.getName(), ecaPolicy);
		}
	}
	
	public ECAPolicy addPolicyFromPFNotification (Policy restPolicy)
	{
		ECAPolicy ecaPolicy = getECAPolicyFromPF(restPolicy);
		return this.optimizer.getPolicies().put(ecaPolicy.getName(), ecaPolicy);
	}
	
	public void buildPolicies (List<TAL> pfPolicies)
	{
		for (TAL talPolicy : pfPolicies)
		{
			buildPolicyFromTAL(talPolicy);
		}
	}
	
	// Utils
	private List<Parameter> buildSensorParameterList(Output sensorOutput, String code)
	{
		List<Parameter> sensorParamList = new ArrayList<Parameter>();
		code += "List<Parameter> sensorParamList = new ArrayList<Parameter>();\n";
		
		for (edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter p : sensorOutput.getParameters())
		{
			Parameter sensorParam = new Parameter (p.getOID(),p.getName(), null, ValueType.STRING);
			sensorParamList.add(sensorParam);
			code += "Parameter sensorParam = new Parameter (\"" + p.getOID() + "\",\"" + p.getName() + "\", null, ValueType.STRING);\n";
			code += "sensorParamList.add(sensorParam);\n";
		}
		
		return sensorParamList;
	}
	
	private String buildSensor (ECAPolicy policy, edu.upc.gco.slcnt.pf.client.rest.tal.model.Sensor talSensor)
	{
		String code = "";
		
		HashMap<String,edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter> confParameters = new HashMap<String,edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter>();
		for (edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter param : talSensor.getConfiguration().getParameters())
			confParameters.put(param.getName(), param);
		
		//Get Type of the Sensor
		edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter type = confParameters.get("Type");
		switch (Sensor.getSensorClass(type.getValue()))
		{
			case DataLake:
			{
				// Event Condition -> Name
				edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter name = confParameters.get("Name");
				Parameter eventParameter = new Parameter (name.getName(), new ValueString(name.getValue()), ValueType.STRING);
				Condition eventCondition = new Condition (ConditionType.EQ, eventParameter);
				policy.setCondition(eventCondition);
				
				code += "Parameter eventParameter = new Parameter (\"" + name.getName()+ "\", new ValueString(\"" + name.getValue() + "\"), ValueType.STRING);\n";
				code += "Condition eventCondition = new Condition (ConditionType.EQ, eventParameter);\n";
				code += "policy.setCondition(eventCondition);\n";
				
				
				SensorDL dlSensor = new SensorDL (talSensor.getDescription(),talSensor.getOid(),Sensor.getSensorType(talSensor.getOid()), buildSensorParameterList(talSensor.getOutput(), code), null, optimizer, policy);
				
				// Set sensor Frequency
				dlSensor.setMonFreq(Integer.parseInt(confParameters.get("Frequency").getValue()));
				
				policy.setSensor(dlSensor);
				code += "SensorDL dlSensor = new SensorDL (\"" + talSensor.getDescription() + "\", \"" + talSensor.getOid() + "\", " + Sensor.getSensorType(talSensor.getOid()) + ", sensorParamList, null, this);\n";
				code += "policy.setSensor(dlSensor);\n";
				
				break;
			}
			case Kafka:
			{
				// Event Condition -> Topic
				edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter topic = confParameters.get("Topic");
				Parameter eventParameter = new Parameter (topic.getName(), new ValueString(topic.getValue()), ValueType.STRING);
				Condition eventCondition = new Condition (ConditionType.EQ, eventParameter);
				policy.setCondition(eventCondition);
				
				code += "Parameter eventParameter = new Parameter (\"" + topic.getName()+ "\", new ValueString(\"" + topic.getValue() + "\"), ValueType.STRING);\n";
				code += "Condition eventCondition = new Condition (ConditionType.EQ, eventParameter);\n";
				code += "policy.setCondition(eventCondition);\n";
				
				SensorKafka kfkSensor = new SensorKafka(talSensor.getDescription(),Sensor.getSensorType(talSensor.getOid()), topic.getValue(),buildSensorParameterList(talSensor.getOutput(), code), null, optimizer);
				policy.setSensor(kfkSensor);
				code += "SensorKafka kfkSensor = new SensorKafka(\"" + talSensor.getDescription() + "\", " + Sensor.getSensorType(talSensor.getOid()) + ", \"" + topic.getValue() + "\", sensorParamList, null, this);\n";
				code += "policy.setSensor(kfkSensor);\n";
				break;
			}
			case REST:
			default:
				break;
		}
		
		//Check Internal Evaluation
		if (confParameters.get("InternalEvaluation") != null)
			policy.getSensor().setInternalEvaluation(confParameters.get("InternalEvaluation").getValue().equals("True"));
		
		return code;
	}
	
	private String buildBootstrap (ECAPolicy policy, Bootstrap bootstrap)
	{
		String code = "";
		for (BootstrapItem bsItem : bootstrap.getBootstrapItem())
		{
			if (bsItem.getSensor() != null)
			{
				policy.setEventType(EventType.sensorInput);
				code += "policy.setEventType(EventType.sensorInput);\n";
				
				code += buildSensor (policy, bsItem.getSensor());
			}
			// TODO: else if (bsItem.getActuator() != null)
		}
		
		return code;
	}
	
	private String buildBootstrap_old (ECAPolicy policy, Bootstrap bootstrap)
	{
		String code = "";
		for (BootstrapItem bsItem : bootstrap.getBootstrapItem())
		{
			if (bsItem.getSensor() != null)
			{
				policy.setEventType(EventType.sensorInput);
				code += "policy.setEventType(EventType.sensorInput);\n";
				
				if (bsItem.getSensor().getConfiguration() != null)
				{
					// Currently supported parameters (assuming just one per sensor to be added to the Event Condition): Name (Data Lake sensors), Topic (Kafka sensors)
					// Event Condition -> From Configuration -> TODO: Assuming just one Parameter
					edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter p = bsItem.getSensor().getConfiguration().getParameters().get(0);
					Parameter eventParameter = new Parameter (p.getName(), new ValueString(p.getValue()), ValueType.STRING);
					Condition eventCondition = new Condition (ConditionType.EQ, eventParameter);
					policy.setCondition(eventCondition);
					
					code += "Parameter eventParameter = new Parameter (\"" + p.getName()+ "\", new ValueString(\"" + p.getValue() + "\"), ValueType.STRING);\n";
					code += "Condition eventCondition = new Condition (ConditionType.EQ, eventParameter);\n";
					code += "policy.setCondition(eventCondition);\n";
					
					//TODO: Add support for other configuration parameters: Type (datalake, kafka), Frequency (== polling interval in ms) for datalake 
				}
				
				// Sensor without Threshold
				if (bsItem.getSensor().getOutput() != null)
				{
					// Parameters
					List<Parameter> sensorParamList = new ArrayList<Parameter>();
					code += "List<Parameter> sensorParamList = new ArrayList<Parameter>();\n";
					
					for (edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter p : bsItem.getSensor().getOutput().getParameters())
					{
						Parameter sensorParam = new Parameter (p.getOID(),p.getName(), null, ValueType.STRING);
						sensorParamList.add(sensorParam);
						code += "Parameter sensorParam = new Parameter (\"" + p.getOID() + "\",\"" + p.getName() + "\", null, ValueType.STRING);\n";
						code += "sensorParamList.add(sensorParam);\n";
					}
					
					switch (Sensor.getSensorClass(bsItem.getSensor().getOid()))
					{
						case DataLake:
							SensorDL dlSensor = new SensorDL (bsItem.getSensor().getDescription(),bsItem.getSensor().getOid(),Sensor.getSensorType(bsItem.getSensor().getOid()), sensorParamList, null, optimizer, policy);
							policy.setSensor(dlSensor);
							code += "SensorDL dlSensor = new SensorDL (\"" + bsItem.getSensor().getDescription() + "\", \"" + bsItem.getSensor().getOid() + "\", " + Sensor.getSensorType(bsItem.getSensor().getOid()) + ", sensorParamList, null, this);\n";
							code += "policy.setSensor(dlSensor);\n";
							break;
						case Kafka:
							SensorKafka kfkSensor = new SensorKafka(bsItem.getSensor().getDescription(),Sensor.getSensorType(bsItem.getSensor().getOid()), bsItem.getSensor().getConfiguration().getParameters().get(0).getValue(),sensorParamList, null, optimizer);
							policy.setSensor(kfkSensor);
							code += "SensorKafka kfkSensor = new SensorKafka(\"" + bsItem.getSensor().getDescription() + "\", " + Sensor.getSensorType(bsItem.getSensor().getOid()) + ", \"" + bsItem.getSensor().getConfiguration().getParameters().get(0).getValue() + "\", sensorParamList, null, this);\n";
							code += "policy.setSensor(kfkSensor);\n";
							break;
						default:
							break;
					}
				}
			}
			// TODO: else if (bsItem.getActuator() != null)
		}
		
		return code;
	}
	
	private String buildSensorThreshold (ECAPolicy policy, Analysis analisys)
	{
		String code = "";
		//List<Condition> thresholdConditionList = new ArrayList<Condition>();
		//code += "List<Condition> thresholdConditionList = new ArrayList<Condition>();\n";
		List<ConditionGroup> thresholdConditionGroupList = new ArrayList<ConditionGroup>();
		code += "List<ConditionGroup> thresholdConditionGroupList = new ArrayList<ConditionGroup>();\n";
		// Thresholds
		int thresholdIndex = 0;
		for (Threshold t : analisys.getThreshold())
		{
			// Threshold
			ConditionGroup thresholdConditionGroup = new ConditionGroup();
			for (Level l : t.getLevels())
			{
				List<Condition> levelConditionList = new ArrayList<Condition>();
				code += "List<Condition> levelConditionList = new ArrayList<Condition>();\n";
				int paramIndex = 0;
				Parameter thresholdParameter = null;
				Condition thresholdCondition = null;
				for (edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter p : l.getParameters())
				{
					if (paramIndex++ == 0)
					{
						thresholdParameter = new Parameter (p.getOID(),p.getName(), new ValueString(p.getValue()), ValueType.STRING);
						thresholdCondition = new Condition (Condition.getConditionType(t.getComparison()), thresholdParameter);
						code += "Parameter(" + paramIndex + ")thresholdParameter = new Parameter (\"" + p.getOID() + "\",\"" + p.getName() + "\", new ValueString(\"" + p.getValue() + "\"), ValueType.STRING);\n";
						code += "Condition thresholdCondition = new Condition (" + Condition.getConditionType(t.getComparison())+ ", thresholdParameter);\n";
					}
					else
					{
						thresholdParameter = new Parameter (p.getOID(),p.getName(), new ValueString(p.getValue()), ValueType.STRING);
						thresholdCondition = new Condition (Condition.getConditionType(t.getComparison()), thresholdParameter,Condition.getOperator(t.getLogical()));
						code += "Parameter thresholdParameter = new Parameter (\"" + p.getOID() + "\",\"" + p.getName() + "\", new ValueString(\"" + p.getValue() + "\"), ValueType.STRING);\n";
						code += "Condition thresholdCondition = new Condition (" + Condition.getConditionType(t.getComparison())+ ", thresholdParameter," + Condition.getOperator(t.getLogical())+ ");\n";
					}
					//thresholdConditionList.add(thresholdCondition);
					//code += "thresholdConditionList.add(thresholdCondition);\n";
					levelConditionList.add(thresholdCondition);
					code += "levelConditionList.add(thresholdCondition);\n";
				}
				thresholdConditionGroup.setConditions(levelConditionList);
				
			}
			
			if (thresholdIndex++ > 0)
			{
				Operator logicalOp = Condition.getOperator(analisys.getBoolOp());
				if (logicalOp != null)
				{
					thresholdConditionGroup.setExternalOp(logicalOp);
					code += "thresholdConditionGroup.setExternalOp(logicalOp);\n";
				}
			}
			thresholdConditionGroupList.add(thresholdConditionGroup);
			code += "thresholdConditionGroupList.add(thresholdConditionGroup);\n";
			//policy.getSensor().setConditions(thresholdConditionList);
			//code += "policy.getSensor().setConditions(thresholdConditionList);\n";
		}
		logger.info("thresholdConditionGroupList size = " + thresholdConditionGroupList.size());
		policy.getSensor().setConditionGroups(thresholdConditionGroupList);
		code += "policy.getSensor().setConditionGroups(thresholdConditionGroupList);\n";
		
		return code;
	}
	
	private ECAPolicy getECAPolicyFromTAL (String policyName, TAL talPolicy)
	{
		if (talPolicy.getTalRules().size() != 1)
		{
			logger.error("One TAL Rule expected!");
			return null;
		}
		
		TALRule talRule = talPolicy.getTalRules().get(0);
		String code = "";
		
		// Create empty Policy
		ECAPolicy policy = new ECAPolicy();
		policy.setName(policyName);
		
		code += "------------ POLICY " + policyName + " ------------\n";
		code += "Policy policy = new Policy()\n";
		code += "policy.setName(\"policyName\")\n";
		
		
		if (talRule.getBootstrap() != null)
		{
			code += buildBootstrap (policy, talRule.getBootstrap());
		}
		else
		{
			policy.setEventType(EventType.VerticalQoE);
			code += "policy.setEventType(EventType.VerticalQoE);\n";
		}
		
		// Reaction
		for (Diagnosis diagnosis : talRule.getReaction().getDiagnosis())
		{
			if (diagnosis.getSymptom() != null)
			{
				if (diagnosis.getSymptom().getAnalysis().getThreshold() != null)
				{
					switch (policy.getEventType())
					{
						case VerticalQoE:
							edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter p = diagnosis.getSymptom().getAnalysis().getThreshold().get(0).getLevels().get(0).getParameters().get(0);
							Parameter eventParameter = new Parameter (p.getName(), new ValueString(p.getValue()), ValueType.STRING);
							Condition eventCondition = new Condition (ConditionType.EQ, eventParameter);
							policy.setCondition(eventCondition);
							code += "Parameter eventParameter = new Parameter (\"" + p.getName()+ "\", new ValueString(\"" + p.getValue() + "\"), ValueType.STRING);\n";
							code += "Condition eventCondition = new Condition (ConditionType.EQ, eventParameter);\n";
							code += "policy.setCondition(eventCondition);\n";
							break;
						case sensorInput:
							code += buildSensorThreshold(policy,diagnosis.getSymptom().getAnalysis());
							// Moved to sensor Configuration (buildSensor)
							/*if (diagnosis.getSymptom().getDescription() != null)
							{
								policy.getSensor().setInternalEvaluation(diagnosis.getSymptom().getDescription().equals("Internal Evaluation"));
								code += "policy.getSensor().setInternalEvaluation(diagnosis.getSymptom().getDescription().equals(\"Internal Evaluation\"));\n";
							}*/
							break;
						default:
							break;
					}
				}
			}
			// TODO: if (diagnosis.getCauses() != null)
		}
		
		for (Tactic tactic : talRule.getReaction().getTactic())
		{
			if (tactic.getAction() != null)
			{
				// Action List
				List<Action> actionList = new ArrayList<Action>();
				code += "List<Action> actionList = new ArrayList<Action>();\n";
				for (ActionOption actionOption : tactic.getAction().getActionOptions())
				{
					// Action
					List<Parameter> actionParams = new ArrayList<Parameter>();
					code += "List<Parameter> actionParams = new ArrayList<Parameter>();\n";
					
					for (edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter p : actionOption.getActuator().getConfiguration().getParameters())
					{
						Parameter actionParam = new Parameter (p.getOID(),p.getName(), new ValueString(p.getValue()), ValueType.STRING);
						actionParams.add(actionParam);
						code += "Parameter actionParam = new Parameter (\"" + p.getOID() + "\",\"" + p.getName() + "\", new ValueString(\"" + p.getValue() + "\"), ValueType.STRING);\n";
						code += "actionParams.add(actionParam);\n";
					}
					Action action = new Action(Action.getActionClass(actionOption.getActuator().getOid()),Action.getActionType(actionOption.getOperation()), actionParams, optimizer.getActController());
					action.setPolicy(policy);
					actionList.add(action);
					code += "Action action = new Action(" + Action.getActionClass(actionOption.getActuator().getOid()) + ", " + Action.getActionType(actionOption.getOperation()) + ", actionParams, this.actController);\n";
					code += "action.setPolicy(policy)\n";
					code += "actionList.add(action);\n";
				}
				policy.setActions(actionList);
				code += "policy.setActions(actionList);\n";
			}
			// TODO: Cause
		}
		
		// Start Sensor Monitor
		if (policy.getSensor() != null)
		{
			policy.getSensor().startMonitor();
			code += "policy.getSensor().startMonitor();\n";
		}
		code += "-----------------------------------------------------\n";
		return policy;
	}
	
	private void buildPolicyFromTAL (TAL tal)
	{
		String code = "";
		
		int policyNumber = 0;
		for (TALRule talRule : tal.getTalRules())
		{
			// Create empty Policy
			ECAPolicy policy = new ECAPolicy();
			policy.setName("Policy-" + (++policyNumber));
			
			code += "------------ POLICY " + policyNumber + " ------------\n";
			code += "Policy policy = new Policy()\n";
			code += "policy.setName(\"Policy-" + policyNumber + "\")\n";
			
			
			if (talRule.getBootstrap() != null)
			{
				code += buildBootstrap (policy, talRule.getBootstrap());
			}
			else
			{
				policy.setEventType(EventType.VerticalQoE);
				code += "policy.setEventType(EventType.VerticalQoE);\n";
			}
			
			// Reaction
			for (Diagnosis diagnosis : talRule.getReaction().getDiagnosis())
			{
				if (diagnosis.getSymptom() != null)
				{
					if (diagnosis.getSymptom().getAnalysis().getThreshold() != null)
					{
						switch (policy.getEventType())
						{
							case VerticalQoE:
								edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter p = diagnosis.getSymptom().getAnalysis().getThreshold().get(0).getLevels().get(0).getParameters().get(0);
								Parameter eventParameter = new Parameter (p.getName(), new ValueString(p.getValue()), ValueType.STRING);
								Condition eventCondition = new Condition (ConditionType.EQ, eventParameter);
								policy.setCondition(eventCondition);
								code += "Parameter eventParameter = new Parameter (\"" + p.getName()+ "\", new ValueString(\"" + p.getValue() + "\"), ValueType.STRING);\n";
								code += "Condition eventCondition = new Condition (ConditionType.EQ, eventParameter);\n";
								code += "policy.setCondition(eventCondition);\n";
								break;
							case sensorInput:
								code += buildSensorThreshold(policy,diagnosis.getSymptom().getAnalysis());
								// Moved to Sensor Configuration
								/*if (diagnosis.getSymptom().getDescription() != null)
								{
									policy.getSensor().setInternalEvaluation(diagnosis.getSymptom().getDescription().equals("Internal Evaluation"));
									code += "policy.getSensor().setInternalEvaluation(diagnosis.getSymptom().getDescription().equals(\"Internal Evaluation\"));\n";
								}*/
								break;
							default:
								break;
						}
					}
				}
				// TODO: if (diagnosis.getCauses() != null)
			}
			
			for (Tactic tactic : talRule.getReaction().getTactic())
			{
				if (tactic.getAction() != null)
				{
					// Action List
					List<Action> actionList = new ArrayList<Action>();
					code += "List<Action> actionList = new ArrayList<Action>();\n";
					for (ActionOption actionOption : tactic.getAction().getActionOptions())
					{
						// Action
						List<Parameter> actionParams = new ArrayList<Parameter>();
						code += "List<Parameter> actionParams = new ArrayList<Parameter>();\n";
						
						for (edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter p : actionOption.getActuator().getConfiguration().getParameters())
						{
							Parameter actionParam = new Parameter (p.getOID(),p.getName(), new ValueString(p.getValue()), ValueType.STRING);
							actionParams.add(actionParam);
							code += "Parameter actionParam = new Parameter (\"" + p.getOID() + "\",\"" + p.getName() + "\", new ValueString(\"" + p.getValue() + "\"), ValueType.STRING);\n";
							code += "actionParams.add(actionParam);\n";
						}
						Action action = new Action(Action.getActionClass(actionOption.getActuator().getOid()),Action.getActionType(actionOption.getOperation()), actionParams, optimizer.getActController());
						action.setPolicy(policy);
						actionList.add(action);
						code += "Action action = new Action(" + Action.getActionClass(actionOption.getActuator().getOid()) + ", " + Action.getActionType(actionOption.getOperation()) + ", actionParams, this.actController);\n";
						code += "action.setPolicy(policy)";
						code += "actionList.add(action);\n";
					}
					policy.setActions(actionList);
					code += "policy.setActions(actionList);\n";
				}
				// TODO: Cause
			}
			
			// Start Sensor Monitor
			if (policy.getSensor() != null)
			{
				policy.getSensor().startMonitor();
				code += "policy.getSensor().startMonitor();\n";
			}
			
			// Add Policy
			//optimizer.getPolicies().add(policy);
			//code += "this.policies.add(policy);\n";
			optimizer.getPolicies().put(policy.getName(), policy);
			code += "optimizer.getPolicies().put(policy.getName(), policy);\n";
			code += "-----------------------------------------------------\n";
		}
		this.logger.info("Policies:\n" + code);
	}
	
	private ECAPolicy getECAPolicyFromPF (Policy restPolicy)
	{
		TAL talPolicy = getTALFromRestPF(restPolicy);
		ECAPolicy ecaPolicy = getECAPolicyFromTAL(restPolicy.getPolicyName(), talPolicy);
		
		return ecaPolicy;
	}
	
	private TAL getTALFromRestPF (Policy pfRestPolicy)
	{
		String strPolicy = pfRestPolicy.getConfig();
		String jsonPolicy = strPolicy;//strPolicy.replace("\\", "");
		logger.info("strPolicy = " + jsonPolicy);
		ObjectMapper mapper = new ObjectMapper();
		Config config = null;
		try {
			config = mapper.readValue(jsonPolicy, Config.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (config != null)
		{
			logger.info("TAL = " + config.getContent().toString());
			return config.getContent();
		}
		return null;
	}
	
	// End Utils
	
	public void buildPolicyFromTAL_block (TAL tal)
	{
		String code = "";
		
		int policyNumber = 0;
		for (TALRule talRule : tal.getTalRules())
		{
			// Create empty Policy
			ECAPolicy policy = new ECAPolicy();
			policy.setName("Policy-" + (++policyNumber));
			
			code += "------------ POLICY " + policyNumber + " ------------\n";
			code += "Policy policy = new Policy()\n";
			code += "policy.setName(\"Policy-" + policyNumber + "\")\n";
			
			
			if (talRule.getBootstrap() != null)
			{
				for (BootstrapItem bsItem : talRule.getBootstrap().getBootstrapItem())
				{
					if (bsItem.getSensor() != null)
					{
						code += "policy.setEventType(EventType.sensorInput);\n";
						
						if (bsItem.getSensor().getConfiguration() != null)
						{
							// Event Condition -> From Configuration
							edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter p = bsItem.getSensor().getConfiguration().getParameters().get(0);
							code += "Parameter eventParameter = new Parameter (\"" + p.getName()+ "\", new ValueString(\"" + p.getValue() + "\"), ValueType.STRING);\n";
							code += "Condition eventCondition = new Condition (ConditionType.EQ, eventParameter);\n";
							code += "policy.setCondition(eventCondition);\n";
						}
						
						// Sensor without Threshold
						if (bsItem.getSensor().getOutput() != null)
						{
							// Parameters
							code += "List<Parameter> sensorParamList = new ArrayList<Parameter>();\n";
							for (edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter p : bsItem.getSensor().getOutput().getParameters())
							{
								code += "Parameter sensorParam = new Parameter (\"" + p.getOID() + "\",\"" + p.getName() + "\", null, ValueType.STRING);\n";
								code += "\nsensorParamList.add(sensorParam);\n";
							}
							
							switch (Sensor.getSensorClass(bsItem.getSensor().getOid()))
							{
								case DataLake:
									code += "SensorDL dlSensor = new SensorDL (\"" + bsItem.getSensor().getDescription() + "\", \"" + bsItem.getSensor().getOid() + "\", " + Sensor.getSensorType(bsItem.getSensor().getOid()) + ", sensorParamList, null, this);\n";
									code += "policy.setSensor(dlSensor);\n";
									break;
								case Kafka:
									code += "SensorKafka kfkSensor = new SensorKafka(\"" + bsItem.getSensor().getDescription() + "\", " + Sensor.getSensorType(bsItem.getSensor().getOid()) + ", sensorParamList, null, this);\n";
									code += "policy.setSensor(kfkSensor);\n";
									break;
								default:
									break;
							}
						}
					}
					// TODO: else if (bsItem.getActuator() != null)
				}
			}
			else
				code += "policy.setEventType(EventType.VerticalQoE);\n";
			
			// Reaction
			for (Diagnosis diagnosis : talRule.getReaction().getDiagnosis())
			{
				if (diagnosis.getSymptom() != null)
				{
					if (diagnosis.getSymptom().getAnalysis().getThreshold() != null)
					{
						code += "List<Condition> thresholdConditionList = new ArrayList<Condition>();\n";
						// Thresholds
						for (Threshold t : diagnosis.getSymptom().getAnalysis().getThreshold())
						{
							// Threshold
							for (Level l : t.getLevels())
							{
								int paramIndex = 0;
								for (edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter p : l.getParameters())
								{
									if (paramIndex == 0)
									{
										code += "Parameter thresholdParameter = new Parameter (\"" + p.getOID() + "\",\"" + p.getName() + "\", new ValueString(\"" + p.getValue() + "\"), ValueType.STRING);\n";
										code += "Condition thresholdCondition = new Condition (" + Condition.getConditionType(t.getComparison())+ ", thresholdParameter);\n";
									}
									else
									{
										code += "Parameter thresholdParameter = new Parameter (\"" + p.getOID() + "\",\"" + p.getName() + "\", new ValueString(\"" + p.getValue() + "\"), ValueType.STRING);\n";
										code += "Condition thresholdCondition = new Condition (" + Condition.getConditionType(t.getComparison())+ ", thresholdParameter," + Condition.getOperator(t.getLogical())+ ");\n";
									}
									code += "thresholdConditionList.add(thresholdCondition);\n";
								}
							}
							code += "policy.getSensor().setConditions(thresholdConditionList);\n";
						}
					}
				}
				// TODO: if (diagnosis.getCauses() != null)
			}
			
			for (Tactic tactic : talRule.getReaction().getTactic())
			{
				if (tactic.getAction() != null)
				{
					// Action List
					code += "List<Action> actionList = new ArrayList<Action>();\n";
					for (ActionOption actionOption : tactic.getAction().getActionOptions())
					{
						// Action
						code += "List<Parameter> actionParams = new ArrayList<Parameter>();\n";
						for (edu.upc.gco.slcnt.pf.client.rest.tal.model.Parameter p : actionOption.getActuator().getConfiguration().getParameters())
						{
							code += "Parameter actionParam = new Parameter (\"" + p.getOID() + "\",\"" + p.getName() + "\", new ValueString(\"" + p.getValue() + "\"), ValueType.STRING);\n";
							code += "actionParams.add(actionParam);\n";
						}
						code += "Action action = new Action(" + Action.getActionClass(actionOption.getActuator().getOid()) + ", " + Action.getActionType(actionOption.getOperation()) + ", actionParams, this.actController);\n";
						code += "actionList.add(action);\n";
					}
				}
				// TODO: Cause
			}
			
			// Create Policy
			code += "policy.setActions(\"actionList\")\n";
			code += "this.policies.add(policy);\n";
			code += "-----------------------------------------------------\n";
		}
		this.logger.info("Policies:\n" + code);
	}
	
	// Static Policies for Testing
	
	private void createQoEFeedbackPolicy1 ()
	{
		// QoE-Feedback Policy 1
		
		// QoE-Feedback Event
		Parameter eventParameter = new Parameter ("QoEFeedBack", new ValueString("VERY BAD"), ValueType.STRING);
		Condition eventCondition = new Condition (ConditionType.EQ, eventParameter);
		
		// QoE-Feedback Action List
		List<Action> actionList = new ArrayList<Action>();
		// QoE-Feedback Action: Increase BW
		List<Parameter> incBwParams = new ArrayList<Parameter>();
		Parameter incBwParam1 = new Parameter("Bandwidth", new ValueInt(40), ValueType.INT);
		incBwParams.add(incBwParam1);
		Parameter incBwParam2 = new Parameter("Segment", new ValueString("RAN"), ValueType.STRING);
		incBwParams.add(incBwParam2);
		
		Action incBw = new Action(ActionClass.QoS, ActionType.increaseBW, incBwParams, optimizer.getActController());
		actionList.add(incBw);
		// QoE-Feedback Policy Creation
		ECAPolicy incBwPolicy = new ECAPolicy("QoE-1", EventType.VerticalQoE, eventCondition, actionList);
		optimizer.getPolicies().put(incBwPolicy.getName(), incBwPolicy);
	}
	
	private void createQoEFeedbackPolicy2 ()
	{
		// QoE-Feedback Policy 2
		
		// QoE-Feedback Event
		Parameter eventParameter = new Parameter ("QoEFeedBack", new ValueString("BAD"), ValueType.STRING);
		Condition eventCondition = new Condition (ConditionType.EQ, eventParameter);
		
		// QoE-Feedback Action List
		List<Action> actionList = new ArrayList<Action>();
		// QoE-Feedback Action: Increase BW
		List<Parameter> incBwParams = new ArrayList<Parameter>();
		Parameter incBwParam1 = new Parameter("Bandwidth", new ValueInt(30), ValueType.INT);
		incBwParams.add(incBwParam1);
		Parameter incBwParam2 = new Parameter("Segment", new ValueString("RAN"), ValueType.STRING);
		incBwParams.add(incBwParam2);
		
		Action incBw = new Action(ActionClass.QoS, ActionType.increaseBW, incBwParams, optimizer.getActController());
		actionList.add(incBw);
		// QoE-Feedback Policy Creation
		ECAPolicy incBwPolicy = new ECAPolicy("QoE-2", EventType.VerticalQoE, eventCondition, actionList);
		optimizer.getPolicies().put(incBwPolicy.getName(),incBwPolicy);
	}
	
	public void createQoEFeedbackPolicies ()
	{
		// QoE Feedback Policies
		createQoEFeedbackPolicy1();
		createQoEFeedbackPolicy2();
	}
	
	private void createNoisyNeighborPolicy ()
	{
		// Noisy Neighbor Policy Definition
		// NN Threshold
		List<Condition> thresholdConditionList = new ArrayList<Condition>();
		//Condition nnc1 = new Condition ("type", ConditionType.EQ, new ValueString("noise"), ValueType.STRING);
		Parameter thresholdParameter1 = new Parameter ("noisyneighbordb.event","type", new ValueString("noise"), ValueType.STRING);
		Condition thresholdCondition1 = new Condition (ConditionType.EQ, thresholdParameter1);
		thresholdConditionList.add(thresholdCondition1);
		//Condition nnc2 = new Condition ("tenantid", ConditionType.EQ, new ValueString("tenantA"), ValueType.STRING);
		Parameter thresholdParameter2 = new Parameter ("noisyneighbordb.event","tenantid", new ValueString("tenantA"), ValueType.STRING);
		Condition thresholdCondition2 = new Condition (ConditionType.EQ, thresholdParameter2, Operator.Logical_AND);
		thresholdConditionList.add(thresholdCondition2);
		
		ConditionGroup thresholdConditionGroup = new ConditionGroup(thresholdConditionList, null);
		
		List<ConditionGroup> thresholdConditionGroupList = new ArrayList<ConditionGroup>();
		thresholdConditionGroupList.add(thresholdConditionGroup);
		
		// NN Sensor
		List<Parameter> sensorParamList = new ArrayList<Parameter>();
		Parameter sensorParam1 = new Parameter ("noisyneighbordb.event","time", null, ValueType.STRING);
		Parameter sensorParam2 = new Parameter ("noisyneighbordb.event","tenantid", null, ValueType.STRING);
		Parameter sensorParam3 = new Parameter ("noisyneighbordb.event","vnfid", null, ValueType.STRING);
		Parameter sensorParam4 = new Parameter ("noisyneighbordb.event","type", null, ValueType.STRING);
		Parameter sensorParam5 = new Parameter ("noisyneighbordb.event","flavor", null, ValueType.STRING);
		sensorParamList.add(sensorParam1);
		sensorParamList.add(sensorParam2);
		sensorParamList.add(sensorParam3);
		sensorParamList.add(sensorParam4);
		sensorParamList.add(sensorParam5);
		SensorDL nnSensor = new SensorDL ("NoisyNeighbor Sensor", "sensor.datalake.noisyneighbordb", SensorType.NoisyNeighbor, sensorParamList, thresholdConditionGroupList, optimizer);
		
		// NN Event Condition
		//Condition nnc3 = new Condition (ConditionType.EQ, new ValueString("NNSensor"), ValueType.STRING);
		Parameter eventParameter = new Parameter ("Name", new ValueString("NoisyNeighbor Sensor"), ValueType.STRING);
		Condition eventCondition = new Condition (ConditionType.EQ, eventParameter);
		
		// NN Action List
		List<Action> actionList = new ArrayList<Action>();
		
		// NN Action: Notify
		List<Parameter> notifyParams = new ArrayList<Parameter>();
		Parameter notifyParam1 = new Parameter ("noisyneighbordb.action","event_ts", new ValueString("{sensor.time}"), ValueType.STRING);
		Parameter notifyParam2 = new Parameter ("noisyneighbordb.action","tenantid", new ValueString("{sensor.tenantid}"), ValueType.STRING);
		notifyParams.add(notifyParam1);
		notifyParams.add(notifyParam2);
		Action notifyDL = new Action(ActionClass.DataLake, ActionType.notify, notifyParams, optimizer.getActController());
		actionList.add(notifyDL);
		
		// NN Action: SSO
		List<Parameter> ssoParams = new ArrayList<Parameter>();
		Parameter ssoParam1 = new Parameter ("noisyneighbordb.event","vnfid", new ValueString("{sensor.vnfid}"), ValueType.STRING);
		//Parameter ssoParam1 = new Parameter ("vnfid", new ValueString("64c9d345-2736-46e9-a6e2-366658e566da"), ValueType.STRING);
		//Parameter ssoParam2 = new Parameter ("tenantid", new ValueString("{sensor.tenantid}"), ValueType.STRING);
		Parameter ssoParam2 = new Parameter ("noisyneighbordb.event","tenantid", new ValueString("h2020-server4"), ValueType.STRING);
		ssoParams.add(ssoParam1);
		ssoParams.add(ssoParam2);
		Action contactSSO = new Action(ActionClass.SSO, ActionType.migrate, ssoParams, optimizer.getActController());
		actionList.add(contactSSO);
		
		// NN Policy Creation
		ECAPolicy nnPolicy = new ECAPolicy("NoisyNeighbor-1", EventType.sensorInput, eventCondition, actionList, nnSensor);
		optimizer.getPolicies().put(nnPolicy.getName(),nnPolicy);
	}
	
	private void createAnomalyDetectionPolicy ()
	{
		// AD Threshold
		List<Condition> thresholdConditionList = new ArrayList<Condition>();
		Parameter thresholdParameter1 = new Parameter ("QoE", new ValueString("1"), ValueType.STRING);
		Condition thresholdCondition1 = new Condition (ConditionType.EQ, thresholdParameter1);
		thresholdConditionList.add(thresholdCondition1);
		
		ConditionGroup thresholdConditionGroup = new ConditionGroup(thresholdConditionList, null);
		
		List<ConditionGroup> thresholdConditionGroupList = new ArrayList<ConditionGroup>();
		thresholdConditionGroupList.add(thresholdConditionGroup);
		
		// AD Sensor
		List<Parameter> sensorParamList = new ArrayList<Parameter>();
		Parameter sensorParam1 = new Parameter ("count", null, ValueType.INT);
		Parameter sensorParam2 = new Parameter ("QoE", null, ValueType.STRING);
		Parameter sensorParam3 = new Parameter ("type", null, ValueType.STRING);
		Parameter sensorParam4 = new Parameter ("imsi", null, ValueType.STRING);
		sensorParamList.add(sensorParam1);
		sensorParamList.add(sensorParam2);
		sensorParamList.add(sensorParam3);
		sensorParamList.add(sensorParam4);
		SensorKafka adSensor = new SensorKafka("AnomalyDetection Sensor", SensorType.AnomalyDetection, "ANOMALYALERT", sensorParamList, thresholdConditionGroupList, optimizer);
		
		// AD Event Condition
		//Parameter eventParameter = new Parameter ("AnomalyDetection Sensor", new ValueString("AnomalyDetection Sensor"), ValueType.STRING);
		Parameter eventParameter = new Parameter ("Topic", new ValueString("ANOMALYALERT"), ValueType.STRING);
		Condition eventCondition = new Condition (ConditionType.EQ, eventParameter);
		
		// AD Action List
		List<Action> actionList = new ArrayList<Action>();
		// TODO: Refine Action
		List<Parameter> ssoParams = new ArrayList<Parameter>();
		Parameter ssoParam1 = new Parameter("imsi", new ValueString("{sensor.imsi}"), ValueType.STRING);
		ssoParams.add(ssoParam1);
		Action adAction = new Action(ActionClass.SSO, ActionType.handOver, ssoParams, optimizer.getActController());
		actionList.add(adAction);
		
		// AD Policy Creation
		ECAPolicy adPolicy = new ECAPolicy("AnomalyDetection-1", EventType.sensorInput, eventCondition, actionList, adSensor);
		optimizer.getPolicies().put(adPolicy.getName(),adPolicy);
	}
	
	private void createFaultDetectionPolicy ()
	{
		// Fault Detection Policy Definition
		// FD Threshold
		List<Condition> thresholdConditionList1 = new ArrayList<Condition>();
		Parameter thresholdParameter1 = new Parameter ("DSP_alarms_db.3082e6b8-b4ec-4aa0-a723-9b313f40ed9e","SEVERITY", new ValueString("critical"), ValueType.STRING);
		Condition thresholdCondition1 = new Condition (ConditionType.EQ, thresholdParameter1);
		thresholdConditionList1.add(thresholdCondition1);
		
		Parameter thresholdParameter2 = new Parameter ("DSP_alarms_db.3082e6b8-b4ec-4aa0-a723-9b313f40ed9e","METRIC_NAME", new ValueString("compute"), ValueType.STRING);
		Condition thresholdCondition2 = new Condition (ConditionType.EQ, thresholdParameter2, Operator.Logical_AND);
		thresholdConditionList1.add(thresholdCondition2);
		
		ConditionGroup thresholdConditionGroup1 = new ConditionGroup(thresholdConditionList1, null);
		
		List<Condition> thresholdConditionList2 = new ArrayList<Condition>();
		Parameter thresholdParameter3 = new Parameter ("DSP_alarms_db.3082e6b8-b4ec-4aa0-a723-9b313f40ed9e","THRESHOLD_VALUE", new ValueString("70"), ValueType.STRING);
		Condition thresholdCondition3 = new Condition (ConditionType.GET, thresholdParameter3, Operator.Logical_AND);
		thresholdConditionList2.add(thresholdCondition3);
		
		ConditionGroup thresholdConditionGroup2 = new ConditionGroup(thresholdConditionList2, Operator.Logical_AND);
		
		List<ConditionGroup> thresholdConditionGroupList = new ArrayList<ConditionGroup>();
		thresholdConditionGroupList.add(thresholdConditionGroup1);
		thresholdConditionGroupList.add(thresholdConditionGroup2);
		
		// FD Sensor
		List<Parameter> sensorParamList = new ArrayList<Parameter>();
		Parameter sensorParam1 = new Parameter ("DSP_alarms_db.3082e6b8-b4ec-4aa0-a723-9b313f40ed9e","time", null, ValueType.STRING);
		Parameter sensorParam2 = new Parameter ("DSP_alarms_db.3082e6b8-b4ec-4aa0-a723-9b313f40ed9e","ALARM_UUID", null, ValueType.STRING);
		Parameter sensorParam3 = new Parameter ("DSP_alarms_db.3082e6b8-b4ec-4aa0-a723-9b313f40ed9e","DSP_ID", null, ValueType.STRING);
		Parameter sensorParam4 = new Parameter ("DSP_alarms_db.3082e6b8-b4ec-4aa0-a723-9b313f40ed9e","GEO_AGG_CODE", null, ValueType.STRING);
		Parameter sensorParam5 = new Parameter ("DSP_alarms_db.3082e6b8-b4ec-4aa0-a723-9b313f40ed9e","METRIC_NAME", null, ValueType.STRING);
		Parameter sensorParam6 = new Parameter ("DSP_alarms_db.3082e6b8-b4ec-4aa0-a723-9b313f40ed9e","VDU_NAME", null, ValueType.STRING);
		Parameter sensorParam7 = new Parameter ("DSP_alarms_db.3082e6b8-b4ec-4aa0-a723-9b313f40ed9e","VNF_MEMBER_INDEX", null, ValueType.STRING);
		sensorParamList.add(sensorParam1);
		sensorParamList.add(sensorParam2);
		sensorParamList.add(sensorParam3);
		sensorParamList.add(sensorParam4);
		sensorParamList.add(sensorParam5);
		sensorParamList.add(sensorParam6);
		sensorParamList.add(sensorParam7);
		SensorDL nnSensor = new SensorDL ("Fault Detection Sensor", "sensor.datalake.DSP_alarms_db", SensorType.FaultDetection, sensorParamList, thresholdConditionGroupList, optimizer);
		
		// FD Event Condition
		Parameter eventParameter = new Parameter ("Name", new ValueString("Fault Detection Sensor"), ValueType.STRING);
		Condition eventCondition = new Condition (ConditionType.EQ, eventParameter);
		
		// FD Action List
		List<Action> actionList = new ArrayList<Action>();
		// FD Action: SSO
		List<Parameter> ssoParams = new ArrayList<Parameter>();
		Parameter ssoParam1 = new Parameter ("DSP_alarms_db.3082e6b8-b4ec-4aa0-a723-9b313f40ed9e","VDU_NAME", new ValueString("{sensor.VDU_NAME}"), ValueType.STRING);
		Parameter ssoParam2 = new Parameter ("DSP_alarms_db.3082e6b8-b4ec-4aa0-a723-9b313f40ed9e","VNF_MEMBER_INDEX", new ValueString("{sensor.VNF_MEMBER_INDEX}"), ValueType.STRING);
		ssoParams.add(ssoParam1);
		ssoParams.add(ssoParam2);
		Action contactSSO = new Action(ActionClass.SSO, ActionType.trafficRedirection, ssoParams, optimizer.getActController());
		actionList.add(contactSSO);
		
		// FD Policy Creation
		ECAPolicy fdPolicy = new ECAPolicy("FaultDetection-1", EventType.sensorInput, eventCondition, actionList, nnSensor);
		optimizer.getPolicies().put(fdPolicy.getName(),fdPolicy);
	}
	
	public void createPolicies ()
	{
		createQoEFeedbackPolicies();
		
		createNoisyNeighborPolicy();
		
		createAnomalyDetectionPolicy();
		
		createFaultDetectionPolicy();
	}
}
