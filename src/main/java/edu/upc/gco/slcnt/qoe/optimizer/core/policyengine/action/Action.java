package edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.action;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import edu.upc.gco.slcnt.qoe.optimizer.core.Actuator;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.ECAPolicy;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.monitoring.SensorDL;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.Parameter;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.Value;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueInt;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueString;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueType;
import edu.upc.gco.slcnt.qoe.optimizer.cp.QoSControl;
import edu.upc.gco.slcnt.qoe.optimizer.cp.SegmentType;
import edu.upc.gco.slcnt.rest.client.sso.interfaces.messages.ActuationRequest;

public class Action {
	private Log logger = LogFactory.getLog(Action.class);
	private ActionClass actionClass;
	private ActionType actionType;
	private List<Parameter> parameters;
	private Actuator controller;
	private ECAPolicy policy;
	
	public Action()
	{
		
	}
	
	public Action (ActionClass actionClass, ActionType actionType, List<Parameter> parameters, Actuator controller)
	{
		this.actionClass = actionClass;
		this.actionType = actionType;
		this.parameters = parameters;
		this.controller = controller;
	}
	
	public static ActionClass getActionClass (String oid)
	{
		if (oid.contains("datalake"))
			return ActionClass.DataLake;
		else if (oid.contains("sso"))
			return ActionClass.SSO;
		else if (oid.contains("cp.qos"))
			return ActionClass.QoS;
		else
			return null;
	}
	
	public static ActionType getActionType (String operation)
	{
		if (operation.equals("Notify"))
			return ActionType.notify;
		else if (operation.equals("Migrate"))
			return ActionType.migrate;
		else if (operation.equals("Resize"))
			return ActionType.resize;
		else if (operation.equals("HandOver"))
			return ActionType.handOver;
		else if (operation.equals("IncreaseBW"))
			return ActionType.increaseBW;
		else if (operation.equals("Redirect"))
			return ActionType.trafficRedirection;
		else
			return null;
	}
	
	private boolean qosActuator ()
	{
		boolean act = false;
		String sliceId = this.controller.getOpt().getQoe().getQoeConf().getSliceId();
		//String cpfURI = this.controller.getControlPlaneFunctionURI(CPSType.QOS);
		QoSControl qosControl = new QoSControl(this.controller.getCpsr(), sliceId);
		
		List<Parameter> valuedParameters = null;
		
		switch (this.actionType)
		{
			case increaseBW:
				/*int delta = ((ValueInt)parameters.get(0).getValue()).getValue();
				String segment = ((ValueString)parameters.get(1).getValue()).getValue();
				//this.controller.getOpt().getQoe().getQoeObj().setValue(delta);
				qosControl.increaseBandwidth(delta, segment);
				*/
				valuedParameters = getValuedParameters();
				qosControl.increaseBandwidth(((ValueInt)valuedParameters.get(0).getValue()).getValue(), ((ValueString)valuedParameters.get(1).getValue()).getValue());
				return true;
			default:
				break;
		}
		
		return act;
	}
	
	private ActuationRequest buildActuationRequest (String actuationName, String description, List<Parameter> parameters)
	{
		String sliceId = this.controller.getOpt().getQoe().getQoeObj().getId();
		Map<String,String> paramMap = new HashMap<String,String>();
		for (Parameter p : parameters)
		{
			paramMap.put(p.getName(), p.getValue().toString());
		}
		ActuationRequest request = new ActuationRequest(sliceId, actuationName, description, paramMap, "");
		logger.info("Actuation Request =\n" + request.toString());
		return request;
	}
	
	private boolean ssoActuator ()
	{
		List<Parameter> valuedParameters = null;
		String str = "";
		
		switch (this.actionType)
		{
			case resize:
				this.controller.getOrchestrator().rescaleVM((((ValueString)parameters.get(0).getValue()).getValue()), (((ValueString)parameters.get(1).getValue()).getValue()));
				return true;
			case migrate:
				/*valuedParameters = getValuedParameters();
				
				this.logger.info("MIGRATE: " + (((ValueString)valuedParameters.get(0).getValue()).getValue()) + " / " + (((ValueString)valuedParameters.get(1).getValue()).getValue()));
				String ret = this.controller.getOrchestrator().migrateVM((((ValueString)valuedParameters.get(0).getValue()).getValue()), (((ValueString)valuedParameters.get(1).getValue()).getValue()));
				logger.info("MIGRATE LOG: " + ret);*/
				valuedParameters = getValuedParameters();
				for (Parameter p : valuedParameters)
				{
					str += p.getName() + "=" + p.getValue().toString() + " ";
				}
				logger.info("MIGRATION of " + str);
				this.controller.getSoDriver().getSoClient().e2eNsiActuation(buildActuationRequest("Migrate", "Live Migration Actuation", valuedParameters));
				return true;
			case handOver:
				valuedParameters = getValuedParameters();
				for (Parameter p : valuedParameters)
				{
					str += p.getName() + "=" + p.getValue().toString() + " ";
				}
				logger.info("HAND OVER of " + str);
				this.controller.getSoDriver().getSoClient().e2eNsiActuation(buildActuationRequest("Handover", "Hand Over Actuation", valuedParameters));
				return true;
			case trafficRedirection:
				valuedParameters = getValuedParameters();
				for (Parameter p : valuedParameters)
				{
					str += p.getName() + "=" + p.getValue().toString() + " ";
				}
				logger.info("TRAFFIC REDIRECTION of " + str);
				this.controller.getSoDriver().getSoClient().e2eNsiActuation(buildActuationRequest("Redirection", "Traffic Redirection Actuation", valuedParameters));
				return true;
			default:
				return false;
		}
	}
	
	private List<Parameter> getValuedParameters ()
	{
		List<Parameter> valuedParameters = new ArrayList<Parameter>();
		logger.info("[getValuedParameters] Parameters size=" + this.parameters.size());
		for (Parameter p : this.parameters)
		{
			Parameter valuedParam = getValuedParameter(p);
			valuedParameters.add(valuedParam);
		}
		
		return valuedParameters;
	}
	
	private String removeBrackets (String inStr)
	{
		int beginIndex = inStr.indexOf('{');
		int endIndex = inStr.indexOf('}');
		return inStr.substring(beginIndex+1, endIndex);
	}
	
	// Get the whole parameter: Name and Value
	private Parameter getValuedParameter (Parameter param)
	{
		logger.info("[getValuedParameter] " + param.toString());
		Parameter valuedParam = new Parameter();
		valuedParam.setName(param.getName());
		valuedParam.setType(param.getType());
		
		if (param.hasConstantValue())
		{
			valuedParam.setValue(param.getValue());
			return valuedParam;
		}
		
		//e.g. {sensor.tenantid}
		String valueStr = removeBrackets(param.getValue().toString());
		//e.g. sensor.tenantid
		int dotIndex = valueStr.indexOf('.');
		String source = valueStr.substring(0, dotIndex);		//e.g. sensor
		String paramName = valueStr.substring(dotIndex + 1);	//e.g. tenantid
		logger.info("[getValuedParameter] " + paramName);
		if (source.equals("sensor"))
		{
			valuedParam.setValue(this.policy.getSensor().getSensorValuedParameter(paramName).getValue());
			logger.info("[getValuedParameter] " + valuedParam.getValue().toString());
		}
		else
			this.logger.error("Malformed value");
		
		return valuedParam;
	}
	
	/*
	// Not used: Use getValuedParameters instead
	private String parseParameterValue (Parameter p)
	{
		if (!p.getType().equals(ValueType.STRING))
			return null;
		
		String paramValue = null;
		String paramStr = ((ValueString) p.getValue()).getValue();
		
		if (p.hasConstantValue())
			return paramStr;
		else
			paramStr = removeBrackets(paramStr);
		
		// e.g.: sensor.tenantid
		int dotIndex = paramStr.indexOf('.');
		String source = paramStr.substring(0, dotIndex);	// e.g.: sensor
		String name = paramStr.substring(dotIndex + 1);		// e.g.: tenantid
		
		if (source.equals("sensor"))
			paramValue = this.policy.getSensor().getSensorParameterValue(name);
		else
			this.logger.error("Malformed value");
		
		this.logger.info("parseParameterValue: " + paramStr + " / " + source + " / " + name);
		return paramValue;
	}
	*/
	private boolean dataLakeActuator ()
	{
		List<Parameter> valuedParameters = null;
		String str = "";
		
		switch (this.actionType)
		{
			case notify:
				switch (this.policy.getSensor().getType())
				{
					case NoisyNeighbor:
						valuedParameters = getValuedParameters();
						for (Parameter p : valuedParameters)
						{
							str += p.getName() + "=" + p.getValue().toString() + " ";
						}
						logger.info("NOTIFY: " + str);
						/*
						// Get values the values of the parameters
						for (Parameter p : this.parameters)
						{
							this.logger.info("Parameter = " + p.toString());
							String value = this.parseParameterValue(p);
							p.setValue(new ValueString(value));
						}*/
						// Execute the Action
						Map<String,Object> fields = new HashMap<String, Object>();
						//for (Parameter p : this.parameters)
						for (Parameter p : valuedParameters)
						{
							this.logger.info("Put: " + p.toString());
							if (p.getName().equals("event_ts"))
							{
								//long t = Long.parseLong(((ValueString)p.getValue()).getValue());
								long t = Instant.parse(((ValueString)p.getValue()).getValue()).toEpochMilli();
								this.logger.info("Put long: " + t);
								fields.put(p.getName(), t);
							}
							else
								fields.put(p.getName(), ((ValueString)p.getValue()).getValue());
							//fields.put("tenantid", nne.getTenantId());
							//fields.put("event_ts", nne.getTime().toEpochMilli());
						}
						String dbName = ((SensorDL) this.policy.getSensor()).getDbName();
						//TODO: Assuming same table for all the parameters
						String tableName = parameters.get(0).getTableFromOID();
						this.logger.info("Insert into " + dbName + "." + tableName);
						this.controller.getOpt().getDataLakeClient().setPoint(dbName, tableName, null, fields);
						break;
					default:
						break;
				}
				
				return true;
			default:
				return false;
		}
	}
	
	public boolean actuate()
	{
		switch (this.actionClass)
		{
			case QoS:
				return this.qosActuator();
			case FGE:
				break;
			case NFConfig:
				break;
			case SSO:
				return this.ssoActuator();
			case DataLake:
				return this.dataLakeActuator();
			default:
				break;
		}
		
		return false;
	}

	public ActionClass getActuatorType() {
		return actionClass;
	}

	public void setActuatorType(ActionClass actionClass) {
		this.actionClass = actionClass;
	}

	public ActionType getActionType() {
		return actionType;
	}

	public void setActionType(ActionType actionType) {
		this.actionType = actionType;
	}

	public List<Parameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}

	public ECAPolicy getPolicy() {
		return policy;
	}

	public void setPolicy(ECAPolicy policy) {
		this.policy = policy;
	}
}
