package edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.action;

public enum ActionClass {
	QoS,
	FGE,
	NFConfig,
	DataLake,
	SSO;
}
