package edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.action;

public enum ActionType {
	increaseBW,
	resize,
	migrate,
	notify,
	handOver,
	trafficRedirection;
}
