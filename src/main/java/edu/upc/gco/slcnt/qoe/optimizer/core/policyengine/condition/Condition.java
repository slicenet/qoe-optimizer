package edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.condition;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import edu.upc.gco.slcnt.qoe.optimizer.core.util.Operator;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.Parameter;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.Value;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueInt;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueString;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueType;

public class Condition {
	
	private Log logger = LogFactory.getLog(Condition.class);
	/*private String parameter;
	private Value value;
	private ValueType vType;*/
	private ConditionType cType;
	private Parameter parameter;
	private Operator externalOp = null;
	
	public Condition()
	{
		
	}
	
	public Condition (ConditionType cType, Parameter parameter)
	{
		this.cType = cType;
		this.parameter = parameter;
	}
	
	public Condition (ConditionType cType, Parameter parameter, Operator externalOp)
	{
		this.cType = cType;
		this.parameter = parameter;
		this.externalOp = externalOp;
	}
	
	public static ConditionType getConditionType (String condition)
	{
		if (condition == null)
			return null;
		
		if (condition.equals("equal"))
			return ConditionType.EQ;
		else if (condition.equals("less"))
			return ConditionType.LT;
		else if (condition.equals("more"))
			return ConditionType.GT;
		else if (condition.equals("lessOrEqual"))
			return ConditionType.LET;
		else if (condition.equals("moreOrEqual"))
			return ConditionType.GET;
		else
			return null;
	}
	
	public static Operator getOperator (String operator)
	{
		if (operator == null)
			return null;
		
		if (operator.equals("and"))
			return Operator.Logical_AND;
		else if (operator.equals("or"))
			return Operator.Logical_OR;
		else
			return null;
	}
	
	private boolean isStringInt (String str)
	{
		try
		{
			int intStr = Integer.parseInt(str);
		}
		catch (NumberFormatException nfe)
		{
			return false;
		}
		
		return true;
	}
	
	public boolean evaluate (Value input)
	{
		boolean opt = false;
		
		switch (this.parameter.getType())
		{
			case INT:
			{
				ValueInt pv = (ValueInt) this.parameter.getValue();
				ValueInt iv = (ValueInt) input;
				switch (this.cType)
				{
					case GT:
						if (iv.getValue() > pv.getValue())
							return true;
						else
							return false;
					case GET:
						if (iv.getValue() >= pv.getValue())
							return true;
						else
							return false;
					case LT:
						if (iv.getValue() < pv.getValue())
							return true;
						else
							return false;
					case LET:
						if (iv.getValue() <= pv.getValue())
							return true;
						else
							return false;
					case EQ:
						if (iv.getValue() == pv.getValue())
							return true;
						else
							return false;
					default:
						break;
				}
				break;
			}
			case STRING:
			{
				ValueString pv = (ValueString) this.parameter.getValue();
				ValueString iv = (ValueString) input;
				switch (this.cType)
				{
					case EQ:
						if (iv.getValue().equals(pv.getValue()))
							return true;
						else
							return false;
					case GT:
						if (isStringInt(pv.getValue()) && isStringInt(iv.getValue()))
							return (Integer.parseInt(iv.getValue()) > Integer.parseInt(pv.getValue()));
						else
						{
							logger.warn("Comparing (>) Strings as Numbers?");
							return false;
						}
					case GET:
						if (isStringInt(pv.getValue()) && isStringInt(iv.getValue()))
							return (Integer.parseInt(iv.getValue()) >= Integer.parseInt(pv.getValue()));
						else
						{
							logger.warn("Comparing (>=) Strings as Numbers?");
							return false;
						}
					case LT:
						if (isStringInt(pv.getValue()) && isStringInt(iv.getValue()))
							return (Integer.parseInt(iv.getValue()) < Integer.parseInt(pv.getValue()));
						else
						{
							logger.warn("Comparing (<) Strings as Numbers?");
							return false;
						}
					case LET:
						if (isStringInt(pv.getValue()) && isStringInt(iv.getValue()))
							return (Integer.parseInt(iv.getValue()) <= Integer.parseInt(pv.getValue()));
						else
						{
							logger.warn("Comparing (<=) Strings as Numbers?");
							return false;
						}
					default:
						break;
				}
				break;
			}
			case BOOL:
				break;
			default:
				break;
		}
		
		return opt;
	}
	
	public Operator getExternalOp() {
		return externalOp;
	}

	public ConditionType getcType() {
		return cType;
	}

	@Override
	public String toString()
	{
		String str = "";
		
		if (this.externalOp != null)
		{
			switch (this.externalOp)
			{
				case Logical_OR:
					str += " or ( ";
					break;
				case Logical_AND:
					str += " and ( ";
					break;
				default:
					break;
			}
		}
		else
			str += " ( ";
		
		switch (this.parameter.getType())
		{
			case INT:
			{	
				str += "\"" + this.parameter.getName() + "\"";
				switch (this.cType)
				{
					case GT:
						str += ">";
						break;
					case GET:
						str += ">=";
						break;
					case LT:
						str += "<";
						break;
					case LET:
						str += "<=";
						break;
					case EQ:
						str += "=";
						break;
					default:
						break;
				}
				str += ((ValueInt) this.parameter.getValue()).getValue();
				break;
			}
			case STRING:
			{
				str += "\"" + this.parameter.getName() + "\"";
				switch (this.cType)
				{
					case EQ:
						str += "=";
						break;
					case GT:
						if (isStringInt(((ValueString)this.parameter.getValue()).getValue()))
							str += ">";
						break;
					case GET:
						if (isStringInt(((ValueString)this.parameter.getValue()).getValue()))
							str += ">=";
						break;
					case LT:
						if (isStringInt(((ValueString)this.parameter.getValue()).getValue()))
							str += "<";
						break;
					case LET:
						if (isStringInt(((ValueString)this.parameter.getValue()).getValue()))
							str += "<=";
						break;
					default:
						break;
				}
				str += "'" + ((ValueString) this.parameter.getValue()).getValue() + "'";
				break;
			}
			case BOOL:
				break;
			default:
				break;
		}
		str += " ) ";
		
		return str;
	}
	/*public Condition (ConditionType cType, Value value, ValueType vType)
	{
		this.cType = cType;
		this.value = value;
		this.vType = vType;
	}
	
	public Condition (String parameter, ConditionType cType, Value value, ValueType vType)
	{
		this.parameter = parameter;
		this.cType = cType;
		this.value = value;
		this.vType = vType;
	}
	
	public boolean evaluate (Value input)
	{
		boolean opt = false;
		
		switch (this.vType)
		{
			case INT:
			{	
				ValueInt pv = (ValueInt) this.value;
				ValueInt iv = (ValueInt) input;
				switch (this.cType)
				{
					case GT:
						if (iv.getValue() > pv.getValue())
							return true;
						else
							return false;
					case LT:
						if (iv.getValue() < pv.getValue())
							return true;
						else
							return false;
					case EQ:
						if (iv.getValue() == pv.getValue())
							return true;
						else
							return false;
					default:
						break;
				}
				break;
			}
			case STRING:
			{
				ValueString pv = (ValueString) this.value;
				ValueString iv = (ValueString) input;
				switch (this.cType)
				{
					case EQ:
						if (iv.getValue().equals(pv.getValue()))
							return true;
						else
							return false;
					default:
						break;
				}
				break;
			}
			case BOOL:
				break;
			default:
				break;
		}
		
		return opt;
	}
	
	public String getParameter() {
		return parameter;
	}
	
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public Value getValue() {
		return value;
	}

	public void setValue(Value value) {
		this.value = value;
	}

	@Override
	public String toString()
	{
		String str = "";
		
		switch (this.vType)
		{
			case INT:
			{	
				str += "\"" + this.parameter + "\"";
				switch (this.cType)
				{
					case GT:
						str += ">";
					case LT:
						str += "<";
					case EQ:
						str += "=";
					default:
						str += ((ValueInt) this.value).getValue();
						break;
				}
				break;
			}
			case STRING:
			{
				str += "\"" + this.parameter + "\"";
				switch (this.cType)
				{
					case EQ:
						str += "=";
					default:
						str += "'" + ((ValueString) this.value).getValue() + "'";
						break;
				}
				break;
			}
			case BOOL:
				break;
			default:
				break;
		}
		
		return str;
	}
	*/

	public Parameter getParameter() {
		return parameter;
	}
}
