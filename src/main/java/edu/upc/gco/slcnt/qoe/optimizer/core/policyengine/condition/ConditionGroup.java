package edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.condition;

import java.util.List;

import edu.upc.gco.slcnt.qoe.optimizer.core.util.Operator;

public class ConditionGroup {

	private List<Condition> conditions;
	private Operator externalOp = null;
	
	public ConditionGroup()
	{
		
	}
	
	public ConditionGroup (List<Condition> conditions, Operator externalOp)
	{
		this.conditions = conditions;
		this.externalOp = externalOp;
	}
	
	public List<Condition> getConditions() {
		return conditions;
	}

	public void setConditions(List<Condition> conditions) {
		this.conditions = conditions;
	}

	public Operator getExternalOp() {
		return externalOp;
	}

	public void setExternalOp(Operator externalOp) {
		this.externalOp = externalOp;
	}
	
	@Override
	public String toString()
	{
		String str = "";
		
		if (this.externalOp != null)
		{
			switch (this.externalOp)
			{
				case Logical_OR:
					str += " or ( ";
					break;
				case Logical_AND:
					str += " and ( ";
					break;
				default:
					break;
			}
		}
		else
			str += " ( ";
		
		for (Condition c : conditions)
			str += c.toString();
		
		str += " ) ";
		
		return str;
	}
}
