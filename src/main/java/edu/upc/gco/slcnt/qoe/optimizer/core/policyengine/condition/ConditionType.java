package edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.condition;

public enum ConditionType {
	GT
	{
		@Override
		public String toString()
		{
			return ">";
		}
	},
	GET
	{
		@Override
		public String toString()
		{
			return ">=";
		}
	},
	LT
	{
		@Override
		public String toString()
		{
			return "<";
		}
	},
	LET
	{
		@Override
		public String toString()
		{
			return "<";
		}
	},
	EQ
	{
		@Override
		public String toString()
		{
			return "=";
		}
	};
}
