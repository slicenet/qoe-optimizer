package edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.event;

import edu.upc.gco.slcnt.qoe.optimizer.core.util.Value;

public class Event {

	private EventType type;
	private Value value;
	private String description;
	
	
	public Event()
	{
		
	}
	
	public Event (EventType type, Value value, String description)
	{
		this.type = type;
		this.value = value;
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public EventType getType() {
		return type;
	}

	public void setType(EventType type) {
		this.type = type;
	}

	public Value getValue() {
		return value;
	}

	public void setValue(Value value) {
		this.value = value;
	}
}
