package edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.monitoring;

import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;

import edu.upc.gco.slcnt.qoe.optimizer.core.Optimizer;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.ECAPolicy;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.condition.Condition;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.condition.ConditionGroup;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.Parameter;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueInt;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueString;

public abstract class Sensor {

	protected Log logger;
	protected String name;
	protected String oid;
	protected SensorClass sensorClass;
	protected SensorType type;
	protected List<Parameter> parameters;
	protected List<ConditionGroup> conditionGroups;
	protected boolean internalEvaluation;
	protected Optimizer optimizer;
	
	public void stop()
	{
		
	}
	
	public Sensor release()
	{
		return null;
	}
	
	public Parameter getSensorValuedParameter (String paramName)
	{
		return null;
	}
	
	//TODO: Can be removed since it is used just by Action.parseParameterValue which is going to be removed
	/*public String getSensorParameterValue (String paramName)
	{
		return null;
	}*/
	
	public Log getLogger() {
		return logger;
	}
	
	public String getName()
	{
		return name;
	}
	
	public SensorClass getSensorClass()
	{
		return sensorClass;
	}
	
	public SensorType getType ()
	{
		return type;
	}
	
	public List<Parameter> getParameters() {
		return parameters;
	}

	/*public List<Condition> getConditions() {
		return conditions;
	}*/
	public List<ConditionGroup> getConditionGroups()
	{
		return conditionGroups;
	}
	
	public void setConditionGroups(List<ConditionGroup> conditionGroups) {
		this.conditionGroups = conditionGroups;
	}

	public boolean isInternalEvaluation() {
		return internalEvaluation;
	}

	public void setInternalEvaluation(boolean internalEvaluation) {
		this.internalEvaluation = internalEvaluation;
	}

	public Optimizer getOptimizer()
	{
		return optimizer;
	}
	
	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}

	public void setParameterValue (String name, String value)
	{
		logger.debug("Setting Parameter: " + name + "=" + value);
		for (Parameter p : parameters)
		{
			logger.debug("Current Parameter: " + p.getName());
			if (p.getName().equals(name))
			{
				switch(p.getType())
				{
					case STRING:
						logger.debug("(String) Value (old/new) = " + ((p.getValue() != null)? p.getValue().toString():"null") + "/" +  value);
						p.setValue(new ValueString(value));
						break;
					case INT:
						logger.debug("(int) Value = " + value);
						p.setValue(new ValueInt(Integer.parseInt(value)));
						break;
					case BOOL:
						break;
				}
			}
		}
	}
	
	public void startMonitor()
	{
		
	}
	
	public static SensorClass getSensorClass (String oid)
	{
		if (oid.contains("datalake"))
			return SensorClass.DataLake;
		else if (oid.contains("kafka"))
			return SensorClass.Kafka;
		else if (oid.contains("rest"))
			return SensorClass.REST;
		else
			return null;
	}
	
	public static SensorType getSensorType (String oid)
	{
		if (oid.contains("NoisyNeighbor"))
			return SensorType.NoisyNeighbor;
		else if (oid.contains("AnomalyDetection"))
			return SensorType.AnomalyDetection;
		else if (oid.contains("FaultDetection"))
			return SensorType.FaultDetection;
		else if (oid.contains("Overload"))
			return SensorType.Overload;
		else
			return null;
	}
}
