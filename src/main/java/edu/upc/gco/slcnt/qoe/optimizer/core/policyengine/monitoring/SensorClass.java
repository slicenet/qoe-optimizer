package edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.monitoring;

public enum SensorClass {
	Kafka,
	DataLake,
	REST;
}
