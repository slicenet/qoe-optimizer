package edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.monitoring;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.influxdb.dto.QueryResult;
import org.influxdb.dto.QueryResult.Series;

import edu.upc.gco.slcnt.influxdb.client.datalake.model.FDAlarmPoint;
import edu.upc.gco.slcnt.influxdb.client.datalake.model.NNEventPoint;
import edu.upc.gco.slcnt.qoe.optimizer.core.Optimizer;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.ECAPolicy;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.condition.Condition;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.condition.ConditionGroup;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.event.Event;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.event.EventType;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.Parameter;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueInt;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueString;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueType;

class DLMonitor implements Runnable
{

	private boolean stop;
	private int freq;
	private SensorDL sensor;
	private Object lastResult = null;
	private boolean internalConditionEvaluation;
	
	public DLMonitor (SensorDL sensor, int freq, boolean internalConditionEvaluation)
	{
		this.freq = freq;
		this.sensor = sensor;
		this.internalConditionEvaluation = internalConditionEvaluation;
	}
	
	private String buildQueryForInternalEvaluation ()
	{
		String query = "select ";
		int i = 1;
		for (Parameter param : this.sensor.getParameters())
		{
			query += param.getName();
			if (i < this.sensor.getParameters().size())
				query += ", ";
			i++;
		}
		//Add the parameters in the select for internal evaluation
		List<String> extraParams = new ArrayList<String>();
		for (ConditionGroup cg : this.sensor.getConditionGroups())
		{
			for (Condition c : cg.getConditions())
			{
				extraParams.add(c.getParameter().getName());
			}
		}
		
		if (extraParams.size() > 0)
			query += ", ";
		i = 1;
		for (String paramName : extraParams)
		{
			query += paramName;
			if (i < extraParams.size())
				query += ", ";
			i++;
		}
		
		// TODO: Assuming same measurement for all parameters
		String tableName = this.sensor.getParameters().get(0).getTableFromOID();
		switch (sensor.getType())
		{
			case NoisyNeighbor:
			case Overload:
			case FaultDetection:
				query += " from \"" + tableName + "\"";
				break;
			default:
				break;
		}
		
		return query;
	}
	
	private String buildQueryForExternalEvaluation ()
	{
		String query = "select ";
		int i = 1;
		for (Parameter param : this.sensor.getParameters())
		{
			query += param.getName();
			if (i < this.sensor.getParameters().size())
				query += ", ";
			i++;
		}
		
		// TODO: Assuming same measurement for all parameters
		String tableName = this.sensor.getParameters().get(0).getTableFromOID();
		switch (sensor.getType())
		{
			case NoisyNeighbor:
			case Overload:
			case FaultDetection:
				query += " from \"" + tableName + "\" where ";
				break;
			default:
				break;
		}
		
		/*for (Condition c : this.sensor.getConditions())
			query += c.toString();*/
		if (this.sensor.getConditionGroups() == null)
			this.sensor.getLogger().info("ConditionGroups is NULL");
		
		for (ConditionGroup cg : this.sensor.getConditionGroups())
		{
			query += cg.toString();
		}
		
		return query;
	}
	
	private String buildQuery ()
	{
		if (internalConditionEvaluation)
			return buildQueryForInternalEvaluation();
		else
			return buildQueryForExternalEvaluation();
	}
	
	private void processLastRecord ()
	{
		Series serie = this.sensor.getOptimizer().getDataLakeClient().getLastResult().getResults().get(0).getSeries().get(0);
		
		for (int i = 0; i < serie.getColumns().size(); i++)
		{
			String paramName = serie.getColumns().get(i);
			String paramValue = serie.getValues().get(0).get(i).toString();
			this.sensor.setParameterValue(paramName, paramValue);
		}
	}
	
	private boolean evaluateLastRecord ()
	{
		boolean totalEvaluation = false;
		boolean firstConditionGroup = true;
		
		switch (sensor.getType())
		{
			case AnomalyDetection:
				return totalEvaluation;
			case FaultDetection:
			{
				FDAlarmPoint lastFda = (FDAlarmPoint) this.lastResult;
				for (ConditionGroup cg : this.sensor.getConditionGroups())
				{
					boolean cgEvaluation = false;
					boolean firstCondition = true;
					for (Condition c : cg.getConditions())
					{
						if (c.getExternalOp() != null)
						{
							switch (c.getExternalOp())
							{
								case Logical_AND:
									if (firstCondition)
									{
										cgEvaluation = true;
										firstCondition = false;
									}
									this.sensor.getLogger().info("Evaluate: " + cgEvaluation + " & " + c.getParameter().getName()  + "  "  + c.getcType().toString() + " " + c.getParameter().toString());
									cgEvaluation &= lastFda.evaluate(c.getParameter().getName(), ((ValueString) c.getParameter().getValue()).getValue(), c.getcType().toString());
									this.sensor.getLogger().info("Evaluate result: " + cgEvaluation);
									break;
								case Logical_OR:
									if (firstCondition)
									{
										cgEvaluation = false;
										firstCondition = false;
									}
									this.sensor.getLogger().info("Evaluate: " + cgEvaluation + " | " + c.getParameter().getName()  + "  "  + c.getcType().toString() + " " + c.getParameter().toString());
									cgEvaluation |= lastFda.evaluate(c.getParameter().getName(), ((ValueString) c.getParameter().getValue()).getValue(), c.getcType().toString());
									this.sensor.getLogger().info("Evaluate result: " + cgEvaluation);
									break;
								default:
									break;
							}
						}
						else
						{
							if (firstCondition)
								firstCondition = false;
							this.sensor.getLogger().info("Evaluate: " + c.getParameter().getName()  + "  "  + c.getcType().toString() + " " + c.getParameter().toString());
							cgEvaluation = lastFda.evaluate(c.getParameter().getName(), ((ValueString) c.getParameter().getValue()).getValue(), c.getcType().toString());
							this.sensor.getLogger().info("Evaluate result: " + cgEvaluation);
						}
					}
					if (cg.getExternalOp() != null)
					{
						switch (cg.getExternalOp())
						{
							case Logical_AND:
								if (firstConditionGroup)
								{
									totalEvaluation = true;
									firstConditionGroup = false;
								}
								totalEvaluation &= cgEvaluation;
								break;
							case Logical_OR:
								if (firstConditionGroup)
								{
									totalEvaluation = false;
									firstConditionGroup = false;
								}
								totalEvaluation |= cgEvaluation;
								break;
							default:
								break;
						}
					}
					else
					{
						if (firstConditionGroup)
							firstConditionGroup = false;
						totalEvaluation = cgEvaluation;
					}
				}
				return totalEvaluation;
			}
			case NoisyNeighbor:
				return totalEvaluation;
			case Overload:
				return totalEvaluation;
			default:
				return totalEvaluation;
		}
	}
	
	private void sendRequest (String query)
	{
		switch (sensor.getType())
		{
			case NoisyNeighbor:
				NNEventPoint nne = this.sensor.getOptimizer().getDataLakeClient().getLastEvent(this.sensor.getDbName(), query, NNEventPoint.class);
				if (nne == null)	// No event in the DB
					break;
				NNEventPoint lastNne = (NNEventPoint) this.lastResult;
				if ((lastNne == null) || (nne.getTime().toEpochMilli() != lastNne.getTime().toEpochMilli()))
				{
					this.lastResult = nne;
					
					// Set Valued Parameters in Sensor
					processLastRecord();
					
					this.sensor.getLogger().info("Generate Event for NNE: " + nne.toString());
					//this.sensor.getOptimizer().generateEvent(EventType.sensorInput, new ValueString(this.sensor.getName()), "NNSensor Event");
					this.sensor.getOptimizer().generateEvent(EventType.sensorInput, this.sensor.getPolicy().getCondition().getParameter().getValue(), "NNSensor Event");
				}
				else
					this.sensor.getLogger().debug("Last Sensor input not modified: " + nne.toString());
				break;
			case FaultDetection:
				FDAlarmPoint fda = this.sensor.getOptimizer().getDataLakeClient().getLastEvent(this.sensor.getDbName(), query, FDAlarmPoint.class);
				if (fda == null)	// No event in the DB
				{
					//this.sensor.getLogger().info("No Fault in DB");
					break;
				}
				FDAlarmPoint lastFda = (FDAlarmPoint) this.lastResult;
				if ((lastFda == null) || (fda.getTime().toEpochMilli() != lastFda.getTime().toEpochMilli()))
				{
					this.sensor.getLogger().info("Generate Event for FDA: " + fda.toString());
					this.lastResult = fda;
					
					if (internalConditionEvaluation)
					{
						boolean totalEvaluation = evaluateLastRecord();
						
						if (!totalEvaluation)
						{
							this.sensor.getLogger().warn("Internal Evaluation is FALSE!");
							break;
						}
					}
					
					// Set Valued Parameters in Sensor
					processLastRecord();
					
					//this.sensor.getOptimizer().generateEvent(EventType.sensorInput, new ValueString(this.sensor.getName()), "FDSensor Event");
					this.sensor.getOptimizer().generateEvent(EventType.sensorInput, this.sensor.getPolicy().getCondition().getParameter().getValue(), "FDSensor Event");
				}
				else
					this.sensor.getLogger().debug("Last Sensor input not modified: " + fda.toString());
				break;
			default:
				break;
		}
	}
	
	public String getParameterValue (String paramName)
	{
		String value = "";
		switch (sensor.getType())
		{
			case NoisyNeighbor:
				this.sensor.getLogger().info("getParameterValue for " + paramName);
				if (paramName.equals("tenantid"))
					value = ((NNEventPoint) this.lastResult).getTenantId();
				else if (paramName.equals("vnfid"))
					value = ((NNEventPoint) this.lastResult).getVnfId();
				else if (paramName.equals("time"))
					value = "" + ((NNEventPoint) this.lastResult).getTime().toEpochMilli();
				this.sensor.getLogger().info( paramName + " = " + value);
				break;
			default:
				break;
		}
		
		return value;
	}
	
	public void stop ()
	{
		this.stop = true;
	}
	
	public void release()
	{
		this.lastResult = null;
		this.sensor = null;
	}
	
	@Override
	public void run()
	{	
		// Wait freq seconds to start - TODO: Find more elegant solution
		/*try {
			TimeUnit.SECONDS.sleep(this.freq);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		String query = this.buildQuery();
		this.sensor.getLogger().info("Sensor Query: " + query);
		while (!stop)
		{
			this.sendRequest(query);
			try {
				//TimeUnit.SECONDS.sleep(this.freq);
				TimeUnit.MILLISECONDS.sleep(this.freq);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public Object getLastResult() {
		return lastResult;
	}

	public void setLastResult(Object lastResult) {
		this.lastResult = lastResult;
	}
}

public class SensorDL extends Sensor {

	private ECAPolicy policy;
	//private int sensorId;		// QoE-O internal id 
	
	private String dbName;
	private String measurement;
	
	private int monFreq = 1000;	//msec
	private DLMonitor monitor;
	private Thread monThread;
	
	/*public Sensor (int sensorId, String name, Optimizer optimizer)
	{
		this.sensorId = sensorId;
		this.name = name;
		this.optimizer = optimizer;
	}*/
	
	//TODO: Remove, I just keep it for the old static policies...
	public SensorDL (String name, String oid, SensorType type, List<Parameter> parameters, List<ConditionGroup> conditionGroups, Optimizer optimizer)
	{
		this.logger = LogFactory.getLog(SensorDL.class);
		this.sensorClass = SensorClass.DataLake;
		this.name = name;
		this.oid = oid;
		this.type = type;
		this.parameters = parameters;
		this.dbName = getDBFromParameter();
		this.conditionGroups = conditionGroups;
		this.optimizer = optimizer;
		this.startMonitor();
	}
	
	public SensorDL (String name, String oid, SensorType type, List<Parameter> parameters, List<ConditionGroup> conditionGroups, Optimizer optimizer, ECAPolicy policy)
	{
		this.logger = LogFactory.getLog(SensorDL.class);
		this.sensorClass = SensorClass.DataLake;
		this.name = name;
		this.oid = oid;
		this.type = type;
		this.parameters = parameters;
		this.dbName = getDBFromParameter();
		this.conditionGroups = conditionGroups;
		this.optimizer = optimizer;
		this.policy = policy;
		//this.startMonitor(5);
	}
	
	/*private void startMonitor (int monFreq)
	{
		this.monitor = new DLMonitor (this, monFreq, isInternalEvaluation());
		this.monThread = new Thread(this.monitor);
		this.monThread.start();
	}*/
	
	private void stopMonitor ()
	{
		this.monitor.stop();
		this.monitor.release();
		this.monThread = null;
		this.monitor = null;
	}
	
	private void generateEvent()
	{
		//TODO
		this.optimizer.generateEvent(EventType.sensorInput, null, "");
	}
	
	// To Delete -> The DB name is extracted from Parameters OID
	private String getDBFromOID ()
	{
		String dbName = "";
		
		StringTokenizer tokenizer = new StringTokenizer (oid, ".");
		tokenizer.nextToken();				// sensor
		tokenizer.nextToken();				// datalake
		dbName = tokenizer.nextToken();		// DB name
		
		return dbName;
	}
	
	private String getDBFromParameter ()
	{
		return this.parameters.get(0).getDBFromOID();
	}
	
	@Override
	public Parameter getSensorValuedParameter (String paramName)
	{
		for (Parameter p : parameters)
		{
			if (p.isSameParameter(paramName))
				return p;
		}
		return null;
	}
	/*
	// TODO: Remove: Not used anymore
	@Override
	public String getSensorParameterValue (String paramName)
	{
		return this.monitor.getParameterValue(paramName);
	}*/
	
	@Override
	public void startMonitor ()
	{
		//this.startMonitor(200);
		this.monitor = new DLMonitor (this, monFreq, isInternalEvaluation());
		this.monThread = new Thread(this.monitor);
		this.monThread.start();
	}
	
	@Override
	public void stop ()
	{
		logger.info("Stopping Sensor");
		this.stopMonitor();
	}
	
	@Override
	public Sensor release ()
	{
		this.parameters.clear();
		this.conditionGroups.clear();
		this.sensorClass = null;
		this.policy = null;
		this.optimizer = null;
		
		return null;
	}

	public ECAPolicy getPolicy() {
		return policy;
	}

	public String getDbName ()
	{
		return dbName;
	}

	public void setMonFreq(int monFreq) {
		this.monFreq = monFreq;
	}
}
