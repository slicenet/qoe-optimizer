package edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.monitoring;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;

import edu.upc.gco.slcnt.qoe.optimizer.core.Optimizer;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.ECAPolicy;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.condition.Condition;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.condition.ConditionGroup;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.event.EventType;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.Parameter;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.Value;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueInt;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueString;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueType;

class KafkaMonitor implements Runnable
{
	private SensorKafka sensor;
	private int alertCounter = 0;
	private List<Parameter> record;
	private KafkaConsumer<String,String> consumer;

	public KafkaMonitor (SensorKafka sensor)
	{
		this.sensor = sensor;
	}
	
	public KafkaMonitor (SensorKafka sensor, KafkaConsumer<String,String> consumer)
	{
		this.sensor = sensor;
		this.consumer = consumer;
	}
	
	public void stop()
	{
		//sensor.getConsumer().wakeup();
		consumer.wakeup();
	}
	
	public void release()
	{
		consumer.unsubscribe();
		consumer = null;
		this.record.clear();
	}
	
	@Override
	public void run() {
		try
		{
			while(true)
			{
				//ConsumerRecords<String,String> records = sensor.getConsumer().poll(100);
				ConsumerRecords<String,String> records = consumer.poll(100);
				for (ConsumerRecord<String,String> record : records)
				{
					sensor.getLogger().info("Topic=" + record.topic() + ", Partition=" + record.partition() + ", Offset=" + record.offset() + ", Key=" + record.key() + ", Value=" + record.value());
					alertCounter++;
					switch (sensor.getType())
					{
						case AnomalyDetection:
							if (!record.topic().contains("ANOMALYALERT"))
							{
								sensor.getLogger().error("Received wrong alert!");
								//break;
							}
						default:
							//processRecord(record.topic(), record.value());
							processRecord(sensor.getType(), record.value());
							break;
					}
				}
				//sensor.getConsumer().commitAsync();
				consumer.commitAsync();
			}
		}
		catch (WakeupException wue)
		{
			sensor.getLogger().info("Received Wake-up !! " + alertCounter + " Alerts processed!");
		}
		finally
		{
			//sensor.getConsumer().close();
			consumer.close();
		}
	}
	
	private String removeBrackets (String inStr)
	{
		int beginIndex = inStr.indexOf('{');
		int endIndex = inStr.indexOf('}');
		return inStr.substring(beginIndex+1, endIndex);
	}
	
	private String getParamName (String inStr)
	{
		int endIndex = inStr.indexOf(':');
		String strWithCommas = inStr.substring(0, endIndex);
		int beginIndex = strWithCommas.indexOf("\"");
		strWithCommas = strWithCommas.substring(beginIndex+1);
		endIndex = strWithCommas.indexOf("\"");
		
		return strWithCommas.substring(0, endIndex);
	}
	
	private Value getParamValueString (String inStr)
	{
		int beginIndex = inStr.indexOf(':');
		String valueWithCommas = inStr.substring(beginIndex+1);
		beginIndex = valueWithCommas.indexOf("\"");
		valueWithCommas = valueWithCommas.substring(beginIndex+1);
		int endIndex = valueWithCommas.indexOf("\"");
		
		return new ValueString(valueWithCommas.substring(0,endIndex));
	}
	
	private Value getParamValueInt (String inStr)
	{
		int beginIndex = inStr.indexOf(':');
		return new ValueInt(Integer.parseInt(inStr.substring(beginIndex+1).replace(" ", "")));
	}
	
	private void getParametersFromString (SensorType sensorType, String alertStr)
	{
		record = new ArrayList<Parameter>();
		StringTokenizer strTokenizer = new StringTokenizer(alertStr, ",");
		
		switch (sensorType)
		{
			case AnomalyDetection:
			{
				//count
				String countStr = strTokenizer.nextToken();
				Parameter count = new Parameter(getParamName(countStr), getParamValueInt(countStr), ValueType.INT);
				record.add(count);
				//QoE
				String qoeStr = strTokenizer.nextToken();
				Parameter qoe = new Parameter(getParamName(qoeStr), getParamValueString(qoeStr), ValueType.STRING);
				record.add(qoe);
				//type
				String typeStr = strTokenizer.nextToken();
				Parameter type = new Parameter(getParamName(typeStr), getParamValueString(typeStr), ValueType.STRING);
				record.add(type);
				//imsi
				String imsiStr = strTokenizer.nextToken();
				Parameter imsi = new Parameter(getParamName(imsiStr), getParamValueString(imsiStr), ValueType.STRING);
				record.add(imsi);
				break;
			}
			case NoisyNeighbor:
			{
				//time
				String timeStr = strTokenizer.nextToken();
				Parameter time = new Parameter(getParamName(timeStr), getParamValueString(timeStr), ValueType.STRING);
				record.add(time);
				//alert
				String alertNameStr = strTokenizer.nextToken();
				Parameter alertName = new Parameter(getParamName(alertNameStr), getParamValueString(alertNameStr), ValueType.STRING);
				record.add(alertName);
				//device
				String deviceStr = strTokenizer.nextToken();
				Parameter device = new Parameter(getParamName(deviceStr), getParamValueString(deviceStr), ValueType.STRING);
				record.add(device);
				//host
				String hostStr = strTokenizer.nextToken();
				Parameter host = new Parameter(getParamName(hostStr), getParamValueString(hostStr), ValueType.STRING);
				record.add(host);
				//type
				String typeStr = strTokenizer.nextToken();
				Parameter type = new Parameter(getParamName(typeStr), getParamValueString(typeStr), ValueType.STRING);
				record.add(type);
				break;
			}
			default:
				break;
		}
	}
	
	private boolean evaluateConditionGroup (ConditionGroup conditionGroup)
	{
		boolean result = false;
		int i = 0;
		
		for (Condition c : conditionGroup.getConditions())
		{
			for (Parameter p : record)
			{
				if (c.getParameter().getName().equals(p.getName()))
				{
					sensor.getLogger().info("Evaluating Condition: " + c.toString() + " with Parameter " + p.toString());
					if (i == 0)
						result = c.evaluate(p.getValue());
					else
					{
						switch (c.getExternalOp())
						{
							case Logical_OR:
								result |= c.evaluate(p.getValue());
								break;
							case Logical_AND:
								result &= c.evaluate(p.getValue());
								break;
							default:
								break;
						}
					}
				}
			}
			i++;
		}
		
		return result;
	}
	
	private boolean conditionsFulfilled ()
	{
		boolean result = false;
		int i = 0;
		
		for (ConditionGroup cg : sensor.conditionGroups)
		{
			if (i == 0)
				result = evaluateConditionGroup (cg);
			else
			{
				switch (cg.getExternalOp())
				{
					case Logical_OR:
						result |= evaluateConditionGroup (cg);
						break;
					case Logical_AND:
						result &= evaluateConditionGroup (cg);
						break;
					default:
						break;
				}
			}
		}
		return result;
	}
	
	private void processRecord (/*String topic*/SensorType sensorType, String alertStr)
	{
		getParametersFromString(sensorType, removeBrackets(alertStr));
		
		//Debug
		sensor.getLogger().info("Record= " + alertStr);
		for (Parameter p : record)
		{
			sensor.getLogger().debug(p.toString());
		}
		
		/*switch (sensorType)
		{
			case AnomalyDetection:*/
				if (conditionsFulfilled())
				{
					for (Parameter p : record)
					{
						sensor.setParameterValue(p.getName(), p.getValue().toString());
					}
					sensor.getLogger().debug("Generate Event for " + sensor.getName());
					this.sensor.getOptimizer().generateEvent(EventType.sensorInput, new ValueString(sensor.getTopic()), sensorType.toString() + " Event");
				}
				/*break;
			default:
				break;
		}*/
	}
	
	public List<Parameter> getRecord ()
	{
		return record;
	}
}

public class SensorKafka extends Sensor {

	//private KafkaConsumer<String,String> consumer;
	private String topic;
	private KafkaMonitor monitor;
	private Thread monThread;
	//private List<Condition> conditions;
	
	public SensorKafka (String name, SensorType type, String topic, List<Parameter> parameters, List<ConditionGroup> conditionGroups, Optimizer optimizer)
	{
		this.logger = LogFactory.getLog(SensorKafka.class);
		this.sensorClass = SensorClass.Kafka;
		this.name = name;
		this.type = type;
		this.topic = topic;
		this.parameters = parameters;
		this.conditionGroups = conditionGroups;
		this.optimizer = optimizer;
		
		/*switch (this.type)
		{
			case AnomalyDetection:
				this.startMonitor(createAnomalyDetectionConsumer());
				break;
			default:
				break;
		}*/
	}
	
	private KafkaConsumer<String,String> createAnomalyDetectionConsumer ()
	{
		Properties props = new Properties();
		props.put("bootstrap.servers", this.optimizer.getKafkaIp() + ":" + this.optimizer.getKafkaPort());
		props.put("group.id", "AnomalyDetection");
		props.put("enable.auto.commit", "false");
		props.put("key.deserializer", StringDeserializer.class.getName());
		props.put("value.deserializer", StringDeserializer.class.getName());
		KafkaConsumer<String,String> consumer  = new KafkaConsumer<String,String>(props);
		consumer.subscribe(Collections.singletonList("ANOMALYALERT"));
		//this.consumer  = new KafkaConsumer<String,String>(props);
		//this.consumer.subscribe(Collections.singletonList("ANOMALYALERT"));
		
		return consumer;
	}
	
	private KafkaConsumer<String,String> createConsumer ()
	{
		Properties props = new Properties();
		props.put("bootstrap.servers", this.optimizer.getKafkaIp() + ":" + this.optimizer.getKafkaPort());
		props.put("group.id", optimizer.getIdentifier().toString());
		props.put("enable.auto.commit", "false");
		props.put("key.deserializer", StringDeserializer.class.getName());
		props.put("value.deserializer", StringDeserializer.class.getName());
		KafkaConsumer<String,String> consumer  = new KafkaConsumer<String,String>(props);
		consumer.subscribe(Collections.singletonList(topic));
		
		return consumer;
	}
	
	private void startMonitor(KafkaConsumer<String,String> consumer)
	{
		this.monitor = new KafkaMonitor (this, consumer);
		this.monThread = new Thread(this.monitor);
		this.monThread.start();
	}
	
	private void stopMonitor()
	{
		this.monitor.stop();
		this.monitor.release();
		this.monThread = null;
		this.monitor = null;
	}
	
	public String getTopic()
	{
		return this.topic;
	}
	
	@Override
	public void startMonitor ()
	{
		/*switch (this.type)
		{
			case AnomalyDetection:
				this.startMonitor(createAnomalyDetectionConsumer());
				break;
			default:
				break;
		}*/
		this.startMonitor(createConsumer());
	}
	
	@Override
	public void stop()
	{
		logger.info("Stopping Sensor");
		this.stopMonitor();
	}
	
	@Override
	public Sensor release()
	{
		//this.consumer.unsubscribe();
		this.parameters.clear();
		this.conditionGroups.clear();
		this.logger = null;
		this.sensorClass = null;
		this.optimizer = null;
		
		return null;
	}
	
	@Override
	public Parameter getSensorValuedParameter (String paramName)
	{
		logger.info("Getting Value for Parameter: " + paramName);
		for (Parameter p : parameters)
		{
			logger.info("Current Parameter: " + p.getName());
			if (p.getName().equals(paramName))
			{
				logger.info("Returning p: " + p.getName() + "==" + paramName + "#");
				return p;
			}
		}
		return null;
	}

	/*public KafkaConsumer<String, String> getConsumer() {
		return consumer;
	}*/
}
