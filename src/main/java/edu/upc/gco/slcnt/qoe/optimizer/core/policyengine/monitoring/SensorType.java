package edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.monitoring;

public enum SensorType {
	NoisyNeighbor
	{
		@Override
		public String toString()
		{
			return "NoisyNeighbor";
		}
	},
	Overload
	{
		@Override
		public String toString()
		{
			return "Overload";
		}
	},
	AnomalyDetection
	{
		@Override
		public String toString()
		{
			return "AnomalyDetection";
		}
	},
	FaultDetection
	{
		@Override
		public String toString()
		{
			return "FaultDetection";
		}
	};
}
