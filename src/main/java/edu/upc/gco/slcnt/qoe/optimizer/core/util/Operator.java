package edu.upc.gco.slcnt.qoe.optimizer.core.util;

public enum Operator {

	Logical_AND,
	Logical_OR;
}
