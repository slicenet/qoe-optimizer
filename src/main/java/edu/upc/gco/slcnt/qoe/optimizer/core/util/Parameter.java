package edu.upc.gco.slcnt.qoe.optimizer.core.util;

import java.util.StringTokenizer;

public class Parameter {

	private String oid;		//Contains the location of the parameter
	private String name;
	private Value value;
	private ValueType type;
	
	public Parameter ()
	{
		
	}
	
	public Parameter (String name, Value value, ValueType type)
	{
		this.name = name;
		this.value = value;
		this.type = type;
	}
	
	public Parameter (String oid, String name, Value value, ValueType type)
	{
		this.oid = oid;
		this.name = name;
		this.value = value;
		this.type = type;
	}

	// Utils
	public boolean hasConstantValue ()
	{
		String valueStr = value.toString();
		
		if (valueStr.contains("{") && valueStr.contains("}"))
			return false;
		else
			return true;
	}
	
	public String getDBFromOID ()
	{
		String dbName = "";
		
		StringTokenizer tokenizer = new StringTokenizer (oid, ".");
		dbName = tokenizer.nextToken();		// DB Name
		
		return dbName;
	}
	
	public String getTableFromOID ()
	{
		String tableName = "";
		
		StringTokenizer tokenizer = new StringTokenizer (oid, ".");
		tokenizer.nextToken();				// DB Name
		tableName = tokenizer.nextToken();	// Table (Measurement)
		
		return tableName;
	}
	
	public boolean isSameParameter (String paramName)
	{
		if (name.equals(paramName) || name.equalsIgnoreCase(paramName.replace(" ", "")) || name.equalsIgnoreCase(paramName.replace("_", "")))
			return true;
		
		return false;
	}
	// End Utils
	
	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Value getValue() {
		return value;
	}
	public void setValue(Value value) {
		this.value = value;
	}
	public ValueType getType() {
		return type;
	}
	public void setType(ValueType type) {
		this.type = type;
	}
	
	@Override
	public String toString()
	{
		String str = "";
		
		str += "Name: " + this.name + " Value: " + ((this.value != null)? this.value.toString():"null");
		
		return str;
	}
}
