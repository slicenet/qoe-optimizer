package edu.upc.gco.slcnt.qoe.optimizer.core.util;

public class ValueInt extends Value {

	private int value;
	
	public ValueInt(int value)
	{
		super();
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	@Override
	public String toString()
	{
		return ""+this.value;
	}
}
