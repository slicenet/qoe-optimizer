package edu.upc.gco.slcnt.qoe.optimizer.core.util;

public enum ValueType {
	INT,
	STRING,
	BOOL
}
