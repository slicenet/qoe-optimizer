package edu.upc.gco.slcnt.qoe.optimizer.cp;

import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.CPSType;

public abstract class CPFunction {

	protected CPSR cpsr;
	protected String sliceId;
	protected String cpfURI = "";
	protected CPSType cpsType;
	
	public CPFunction()
	{
		
	}
	
	public CPFunction(CPSR cpsr, String sliceId, CPSType cpsType)
	{
		this.cpsr = cpsr;
		this.sliceId = sliceId;
		this.cpsType = cpsType;
		this.cpfURI = this.getControlPlaneFunctionURI(sliceId, cpsType);
	}
	
	// Get the QoS-C function for the Slice from the CPSR
	private String getControlPlaneFunctionURI (String sliceId, CPSType cpstType)
	{
		//return this.cpfURI = this.cpsr.requestFunction(sliceId, cpstType.getValue());
		return "";
	}
}
