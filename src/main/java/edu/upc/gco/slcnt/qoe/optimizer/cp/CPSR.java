package edu.upc.gco.slcnt.qoe.optimizer.cp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upc.gco.slcnt.rest.client.cp.cpsr.CPSRApi;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.CPSProfile;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.CPSRegistrationData;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.CPSService;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.CPSStatus;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.CPSType;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.DefaultNotificationSubscription;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.IpEndPoint;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.ProblemDetails;

class HeartBeatTimer implements Runnable
{
	private boolean stop = false;
	private int hbTime;				//seconds
	private CPSR cpsr;
	
	public HeartBeatTimer (CPSR cpsr, int hbTime)
	{
		this.hbTime = hbTime;
		this.cpsr = cpsr;
	}
	
	private HttpStatus sendHeartBeat ()
	{
		return this.cpsr.getCpsrClient().sendHeartBeat(this.cpsr.getBaseUrl(), this.cpsr.getCpsProfile().getInstanceId());
	}
	
	public void stop()
	{
		this.stop = true;
	}
	
	@Override
	public void run() {
		
		while (!stop)
		{
			if (sendHeartBeat() != HttpStatus.OK)
			{
				this.stop();
				this.cpsr.resetHearBeatTimer();
			}
			else
			{
				try {
					TimeUnit.MILLISECONDS.sleep(hbTime*995);
					//TimeUnit.SECONDS.sleep(hbTime);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}	
}

public class CPSR {
	
	private String registryIp;
	private String registryPort;
	private String baseUrl;
	private Log logger = LogFactory.getLog(CPSR.class);
	private CPSRApi cpsrClient;
	private CPSProfile cpsProfile;
	private List<CPSService> cpsServices = null;
	private HeartBeatTimer hbTimer;
	private Thread hbThread;
	
	public CPSR()
	{
		
	}
	
	public CPSR (String ip, String port)
	{
		this.registryIp = ip;
		this.registryPort = port;
		this.baseUrl = "http://" + this.registryIp + ":" + this.registryPort;
		this.cpsServices = new ArrayList<CPSService>();
		this.cpsrClient = new CPSRApi();
	}
	
	private void startHeartBeatTimer (int hbTime)
	{
		this.hbTimer = new HeartBeatTimer (this, hbTime);
		this.hbThread = new Thread (this.hbTimer);
		this.hbThread.start();
	}
	
	private void stopHeartBeatTimer ()
	{
		this.hbTimer.stop();
		this.hbThread = null;
		this.hbTimer = null;
	}
	
	protected void resetHearBeatTimer ()
	{
		this.stopHeartBeatTimer();
		logger.info("Re-Registering QoE Optimizer...");
		this.registerOptimizer();
	}
	
	public void createCPSService (String name, String version, String schema, UUID sliceId, String fqdn, String endPointIP, String endPointTransport, String endPointPort, String apiPrefix, List<DefaultNotificationSubscription> subscriptions, List<CPSType> allowedTypes, List<UUID> allowedSlices, int capacity, int load)
	{
		CPSService cpsService = new CPSService();
		
		cpsService.setInstanceId(UUID.randomUUID());
		cpsService.setName(name);
		cpsService.setVersion(version);
		cpsService.setSchema(schema);
		cpsService.setSliceId(sliceId);
		cpsService.setFqdn(fqdn);
		
		// Create EndPoint
		//cpsService.setEndPoints(endPoints);
		ArrayList<IpEndPoint> endPoints = new ArrayList<IpEndPoint>();
		IpEndPoint endPoint = new IpEndPoint();
		endPoint.setIpv4Address(endPointIP);
		endPoint.setTransport(endPointTransport);
		endPoint.setPort(Integer.parseInt(endPointPort));
		endPoints.add(endPoint);
		cpsService.setEndPoints(endPoints);
		
		cpsService.setApiPrefix(apiPrefix);
		cpsService.setSubscriptions(subscriptions);
		cpsService.setAllowedTypes(allowedTypes);
		cpsService.setAllowedSlices(allowedSlices);
		cpsService.setCapacity(capacity);
		cpsService.setLoad(load);
		
		this.cpsServices.add(cpsService);
	}
	
	private ArrayList<String> createServiceIpList()
	{
		ArrayList<String> ipv4List = new ArrayList<String>();
		
		for (CPSService s : this.cpsServices)
		{
			for (IpEndPoint e : s.getEndPoints())
			{
				//TODO: Check duplicates
				ipv4List.add(e.getIpv4Address());
			}
		}
		
		return ipv4List;
	}
	
	public void createCPSProfile (UUID optimizerId, UUID sliceId)
	{
		this.cpsProfile = new CPSProfile();
		
		cpsProfile.setInstanceId(optimizerId);
		cpsProfile.setType(CPSType.QOE);
		cpsProfile.setStatus(CPSStatus.REGISTERED);
		cpsProfile.setSliceId(sliceId);
		cpsProfile.setFqdn(null);
		cpsProfile.setIpv4Addresses(this.createServiceIpList());
		cpsProfile.setIpv6Addresses(null);
		cpsProfile.setIpv6Prefixes(null);
		cpsProfile.setCapacity(0);
		cpsProfile.setLoad(0);
		cpsProfile.setServices(this.cpsServices);
	}
	
	public void registerOptimizer ()
	{
		ResponseEntity<String> re = this.cpsrClient.registerCPSInstance(this.baseUrl, this.cpsProfile);
		
		ObjectMapper mapper = new ObjectMapper();
		switch (re.getStatusCodeValue())
		{
			case 200:
				CPSProfile replace = null;
				try {
					replace = mapper.readValue(re.getBody(), CPSProfile.class);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					break;
				}
				logger.info("Replacing the CPS Profile");
				cpsProfile = replace;
				break;
			case 201:
				CPSRegistrationData registration = null;
				try {
					registration = mapper.readValue(re.getBody(), CPSRegistrationData.class);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					break;
				}
				logger.info("Heart Beat Timer: " + registration.getHeartBeatTimer());
				this.startHeartBeatTimer(registration.getHeartBeatTimer());
				break;
			case 400:
			case 500:
			default:
				ProblemDetails problem = null;
				try {
					problem = mapper.readValue(re.getBody(), ProblemDetails.class);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					break;
				}
				logger.info("Problem: " + problem.getStatus());
				break;
		}
		/*
		String url = this.baseUrl + "/slicenet/ctrlplane/cpsr_cps/v1/cps-instances/" + this.cpsProfile.getInstanceId();
		
		RestTemplate rt = new RestTemplate(this.getClientHttpRequestFactory());
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	
		HttpEntity<CPSProfile> request = new HttpEntity<CPSProfile>(this.cpsProfile, headers);
		logger.info("PUT: " + url);
		logger.info("JSON: " + request.toString());
		ResponseEntity<Void> re = rt.exchange(url, HttpMethod.PUT, request, Void.class);
		logger.info("Response: " + re.getStatusCodeValue());
		switch (re.getStatusCodeValue())
		{
			case 200:
				//ResponseEntity<CPSProfile> replace = (ResponseEntity<CPSProfile>)re;
				//logger.info("New CPSProfile: " + replace.getBody());
				break;
			case 201:
				//ResponseEntity<CPSRegistrationData> registration = (ResponseEntity<CPSRegistrationData>)re;
				//logger.info("Heart Beat Timer: " + registration.getBody().getHeartBeatTimer());
				break;
			case 400:
			case 500:
			default:
				//ResponseEntity<ProblemDetails>	problem = (ResponseEntity<ProblemDetails>)re;
				//logger.info("Problem: " + problem.getBody().getStatus());
				break;
		}
		*/
	}
	
	public void unregisterOptimizer ()
	{
		logger.info("Unregistering QoE CPS from CPSR");
		this.cpsrClient.deregisterCPSInstance(this.getBaseUrl(), this.getCpsProfile().getInstanceId());
	}

	public String getRegistryIp() {
		return registryIp;
	}

	public void setRegistryIp(String registryIp) {
		this.registryIp = registryIp;
	}

	public String getRegistryPort() {
		return registryPort;
	}

	public void setRegistryPort(String registryPort) {
		this.registryPort = registryPort;
	}

	public CPSRApi getCpsrClient() {
		return cpsrClient;
	}

	public void setCpsrClient(CPSRApi cpsrClient) {
		this.cpsrClient = cpsrClient;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public CPSProfile getCpsProfile() {
		return cpsProfile;
	}

	public void setCpsProfile(CPSProfile cpsProfile) {
		this.cpsProfile = cpsProfile;
	}
}
