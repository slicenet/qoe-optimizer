package edu.upc.gco.slcnt.qoe.optimizer.cp;

import edu.upc.gco.slcnt.cp.cps.Configuration;
import edu.upc.gco.slcnt.cp.cps.QoSService;
import edu.upc.gco.slcnt.cp.cps.Configuration.ConfType;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.CPSType;

public class QoSControl extends CPFunction {

	private QoSService qosService;
	
	public QoSControl()
	{
		super();
	}
	
	public QoSControl(CPSR cpsr, String sliceId)
	{
		super (cpsr, sliceId, CPSType.QOS);
		this.qosService = new QoSService(this.cpfURI);
	}
	
	// Future REST API to the QoS Control Function of the SliceNet CP: POST http://this.cpfURI/...
	// Technology Agnostic API (access.json): PUT /access/qos/constraints
	public void increaseBandwidth (int value, String segment)
	{
		//this.qosService.increaseBandwidth(sliceId, value, segmentType);
		Configuration qosConf = new Configuration("Bandwidth", ""+value, "%", ConfType.BW);
		
		if (segment.equals("RAN"))
			this.qosService.configureQoS(sliceId, qosConf, SegmentType.RAN);
	}
}
