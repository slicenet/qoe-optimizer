package edu.upc.gco.slcnt.qoe.optimizer.cp;

public enum SegmentType {
	RAN,
	MEC,
	CORE,
	WAN;
}
