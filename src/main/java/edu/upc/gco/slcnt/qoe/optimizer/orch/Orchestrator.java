package edu.upc.gco.slcnt.qoe.optimizer.orch;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openstack4j.api.OSClient.OSClientV3;
import org.openstack4j.api.client.IOSClientBuilder;
import org.openstack4j.core.transport.Config;
import org.openstack4j.model.common.ActionResponse;
import org.openstack4j.model.common.Identifier;
import org.openstack4j.model.compute.Flavor;
import org.openstack4j.model.compute.Server;
import org.openstack4j.model.compute.actions.LiveMigrateOptions;
import org.openstack4j.openstack.OSFactory;

public class Orchestrator {

	// OpenStack
	private String keystoneIp;
	private String keystonePort;
	private String keystoneVersion;
	private String tenantName;
	private String domainName;
	private String userName;
	private String userPwd;
	private OSClientV3 osClient;
	
	public Orchestrator ()
	{
		
	}
	
	public Orchestrator (String keystoneProto, String keystoneIp, String keystonePort, String keystoneVersion, String tenantName, String domainName, String userName, String userPwd)
	{
		this.keystoneIp = keystoneIp;
		this.keystonePort = keystonePort;
		this.keystoneVersion = keystoneVersion;
		this.tenantName = tenantName;
		this.domainName = domainName;
		this.userName = userName;
		this.userPwd = userPwd;
		
		if (keystoneProto.equals("http"))
			this.osClient = OSFactory.builderV3()
	                .endpoint(keystoneProto + "://" + this.keystoneIp + ":" + this.keystonePort + "/" + this.keystoneVersion)
	                .credentials(this.userName, this.userPwd,Identifier.byName(this.domainName))
	                .scopeToProject(Identifier.byName(this.tenantName), Identifier.byName(this.domainName))
	                .authenticate();
		else if (keystoneProto.equals("https"))
		{
			// Disable SSL Cert verification
			Config config = Config.newConfig().withSSLVerificationDisabled();
			this.osClient = OSFactory.builderV3()
	                .withConfig(config)
	                .endpoint(keystoneProto + "://" + this.keystoneIp + ":" + this.keystonePort + "/" + this.keystoneVersion)
	                .credentials(this.userName, this.userPwd,Identifier.byName(this.domainName))
	                .scopeToProject(Identifier.byName(this.tenantName), Identifier.byName(this.domainName))
	                .authenticate();
		}
	}
	
	public void rescaleVM (String vmId, String flavorName)
	{
		String fName = "";
		String flavorId = "";
		
		List<? extends Flavor> flavors = this.osClient.compute().flavors().list();
		
		Iterator<? extends Flavor> it = flavors.iterator();
		
		while(it.hasNext() && !fName.equals(flavorName)) {
			
			Flavor f = it.next();
			
			fName = f.getName();
			
			if(fName.equals(flavorName)) {
				flavorId = f.getId();
			}
		}
		
		this.osClient.compute().servers().resize(vmId, flavorId);
		
		this.osClient.compute().servers().waitForServerStatus(vmId, Server.Status.VERIFY_RESIZE, 60, TimeUnit.MINUTES);
		
		this.osClient.compute().servers().confirmResize(vmId);
		
	}
	
	public String migrateVM (String vmId, String hostId)
	{		
		LiveMigrateOptions options = LiveMigrateOptions.create()
									.blockMigration(false)
									.diskOverCommit(false)
									.host(hostId);
		
		ActionResponse response = this.osClient.compute().servers().liveMigrate(vmId, options);
		
		if(response.isSuccess()) {
			return "Successful";
		}
		else {
			return response.getFault();
		}
	}
}
