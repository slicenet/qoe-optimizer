package edu.upc.gco.slcnt.qoe.optimizer.orch;

import edu.upc.gco.slcnt.rest.client.sso.ServiceOrchestratorClient;

public class ServiceOrchestratorDriver {

	private ServiceOrchestratorClient soClient;
	
	public ServiceOrchestratorDriver (String sliceId, /*String soLCMServiceUrl*/String protocol, String ipAddress, String port, String user, String password)
	{
		soClient = new ServiceOrchestratorClient (sliceId, /*soLCMServiceUrl*/protocol, ipAddress, port, user, password);
	}

	public ServiceOrchestratorClient getSoClient() {
		return soClient;
	}
}
