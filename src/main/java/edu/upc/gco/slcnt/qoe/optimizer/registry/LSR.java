package edu.upc.gco.slcnt.qoe.optimizer.registry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import edu.upc.gco.slcnt.qoe.optimizer.core.util.KeyValuePair;

public class LSR {

	private Log logger = LogFactory.getLog(LSR.class);
	
	private String registryProto;
	private String registryIp;
	private String registryPort;
	private String baseUrl;
	
	private RestTemplate restTemplate = new RestTemplate();
	private String lsrManageService = "/plug-and-play-manager/lsr/updates";
	private String lsrGetService = "/plug-and-play-manager/lsr/get-service";
	
	public LSR()
	{
		
	}
	
	public LSR (String registryProto, String registryIp, String registryPort)
	{
		this.registryProto = registryProto;
		this.registryIp = registryIp;
		this.registryPort = registryPort;
		
		this.baseUrl = this.registryProto + "://" + this.registryIp + ":" + this.registryPort;
	}
	
	public /*List<KeyValuePair>*/HashMap<String,String> getService (ServiceType type)
	{
		String url = "";
		//ResponseEntity<List<KeyValuePair>> response = null;
		ResponseEntity<HashMap<String,String>> response = null;
		HttpHeaders headers = new HttpHeaders();
		List<MediaType> acceptList = new ArrayList<MediaType>();
		acceptList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptList);
		HttpEntity<Void> request = new HttpEntity<Void>(headers);
		
		switch (type)
		{
			case SS_O:
			case DATA_LAKE:
			case KAFKA_BUS:
			case POLICY_FRAMEWORK:
				url = baseUrl + lsrGetService + "/?service_type=" + type.getValue();
				logger.info("url = " + url);
				//response = restTemplate.exchange(url, HttpMethod.GET, request, new ParameterizedTypeReference<List<KeyValuePair>>() {});
				response = restTemplate.exchange(url, HttpMethod.GET, request, new ParameterizedTypeReference<HashMap<String,String>>() {});
				break;
			case QOE_O:
			default:
				break;
		}
		
		if (response == null)
			logger.error("No Response obtained");
		
		return response.getBody();
	}
}
