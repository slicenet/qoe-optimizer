package edu.upc.gco.slcnt.qoe.optimizer.registry;

public enum ServiceType {
	SS_O				("SSO"),
	KAFKA_BUS			("KAFKA_BUS"),
	DATA_LAKE			("DATA_LAKE"),
	POLICY_FRAMEWORK	("POLICY_FRAMEWORK"),
	QOE_O				("QOE-O");
	
	private String value;
	
	private ServiceType (String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return this.value;
	}
}
