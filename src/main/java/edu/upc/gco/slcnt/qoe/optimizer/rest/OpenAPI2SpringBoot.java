package edu.upc.gco.slcnt.qoe.optimizer.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;


@SpringBootApplication
@ComponentScan(basePackages = {"edu.upc.gco.slcnt.qoe.optimizer.rest", "edu.upc.gco.slcnt.qoe.optimizer.rest.api" , "edu.upc.gco.slcnt.qoe.optimizer.rest.configuration"})
public class OpenAPI2SpringBoot implements CommandLineRunner {

	public QoE qoe = new QoE();
	@Autowired
	private Environment env;
    
    @Override
    public void run(String... arg0) throws Exception {
        if (arg0.length > 0 && arg0[0].equals("exitcode")) {
            throw new ExitException();
        }
        
        // Initialize QoE-Optimizer by env variables instead of REST
        this.qoe.initQoE();
    }
    
    @EventListener
    public void handleContextClosedEvent (ContextClosedEvent cce)
    {
    	if (this.qoe != null)
    	{
    		this.qoe.unregisterQoECPS();
    		if (this.qoe.getQoeOpt() != null)
    		{
    			this.qoe.getQoeOpt().stop();
    			this.qoe.getQoeOpt().release();
    			this.qoe.setQoeOpt(null);
    		}
    	}
    }
    
    public static void main(String[] args) throws Exception {
        new SpringApplication(OpenAPI2SpringBoot.class).run(args);
    }

    class ExitException extends RuntimeException implements ExitCodeGenerator {
        private static final long serialVersionUID = 1L;

        @Override
        public int getExitCode() {
            return 10;
        }

    }

    @Bean
    public WebMvcConfigurer webConfigurer() {
        return new WebMvcConfigurer() {
            /*@Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("*")
                        .allowedMethods("*")
                        .allowedHeaders("Content-Type");
            }*/
        };
    }
    
    // Programatically modify the Listening port through dedicated env variable, different from server.port
    @Bean
    public ConfigurableServletWebServerFactory webServerFactory()
    {
    	TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
    	factory.setPort(Integer.parseInt(env.getProperty("port")));
    	
    	return factory;
    }
    
    @Bean
    public QoE getQoE ()
    {
    	return this.qoe;
    }

}
