package edu.upc.gco.slcnt.qoe.optimizer.rest;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import edu.upc.gco.slcnt.influxdb.client.datalake.DataLakeApi;
import edu.upc.gco.slcnt.pf.client.PFApi;
import edu.upc.gco.slcnt.qoe.optimizer.core.Optimizer;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.event.Event;
import edu.upc.gco.slcnt.qoe.optimizer.core.policyengine.event.EventType;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.KeyValuePair;
import edu.upc.gco.slcnt.qoe.optimizer.core.util.ValueString;
import edu.upc.gco.slcnt.qoe.optimizer.cp.CPSR;
import edu.upc.gco.slcnt.qoe.optimizer.orch.ServiceOrchestratorDriver;
import edu.upc.gco.slcnt.qoe.optimizer.registry.LSR;
import edu.upc.gco.slcnt.qoe.optimizer.registry.ServiceType;
import edu.upc.gco.slcnt.qoe.optimizer.rest.model.QoEConf;
import edu.upc.gco.slcnt.qoe.optimizer.rest.model.QoEObject;

public class QoE {
	
	private QoEConf qoeConf;
	private QoEObject qoeObj;
	private Optimizer qoeOpt = null;
	private CPSR cpsr = null;
	private LSR lsr = null;
	private UUID sliceId;
	@Autowired
	private Environment env;
	
	public QoE ()
	{
		//this.cpsr = new CPSR();
		this.qoeConf = new QoEConf();
		this.qoeObj = new QoEObject();
		//this.qoeObj.setId("-1");
		//this.qoeObj.setValue("Unknown");
		//this.qoeOpt = new Optimizer (this);
		//this.createQoECPS();
		//this.registerOptimizer();
	}
	
	// This operation initializes the QoE Optimizer after the initial configuration is received from the Orchestration/Management
	public void initQoE (String sliceId, String registryIP, String registryPort)
	{
		this.cpsr = new CPSR(registryIP, registryPort);
		
		this.qoeObj.setId(sliceId);
		this.qoeObj.setValue("Unknown");
		
		this.qoeOpt = new Optimizer (this);
		this.qoeOpt.setServerIp(env.getProperty("server.ip"));
		this.qoeOpt.setServerPort(env.getProperty("port"));
		
		this.sliceId = UUID.fromString(sliceId);
		
		this.registerQoECPS();
	}
	
	// Initialization based on Environment variables (for P&P Manager based deployment)
	public void initQoE()
	{
		String mode = env.getProperty("qoe.mode");
		String registry = env.getProperty("qoe.registry");
		
		String cpsrIp = null;
		String sliceId = null;
		String serverIp = null;
		String serverPort = null;
		
		if (mode.equals("P&P"))
		{
			cpsrIp = env.getProperty("cpsrIP");
			sliceId = env.getProperty("slicenetId");
			serverIp = env.getProperty("hostIP");
			serverPort = env.getProperty("port");
		}
		else if (mode.equals("Standalone"))
		{
			cpsrIp = env.getProperty("cpsrIp");
			sliceId = env.getProperty("sliceId");
			serverIp = env.getProperty("server.ip");
			serverPort = env.getProperty("port");
		}
		
		String cpsrPort = env.getProperty("cpsrPort");
		
		this.cpsr = new CPSR(cpsrIp, cpsrPort);
		
		this.qoeObj.setId(sliceId);
		this.qoeObj.setValue("Unknown");
		
		String pfIp = null;
		String pfPort = null;
		String pfProto = null; // TODO: Add env variable
		String dlIp = null;
		String dlPort = null;
		String kfkIp = null;
		String kfkPort = null;
		/*String ksProto = null;
		String ksIp = null;
		String ksPort = null;
		String ksVersion = null;
		String tenantName = null;
		String domainName = null;
		String userName = null;
		String userPwd = null;*/
		String ssoProto = null;
		String ssoIp = null;
		String ssoPort = null;
		String ssoUser = null;
		String ssoPwd = null;
		
		if (registry.equals("LSR"))
		{
			String lsrProto = env.getProperty("lsrProto");	// http
			String lsrIp = env.getProperty("lsrIP");
			String lsrPort = env.getProperty("lsrPort");	// 50003
			
			this.lsr = new LSR(lsrProto, lsrIp, lsrPort);
			
			//List<KeyValuePair> paramList = null;
			HashMap<String,String> paramList = null;
			String protocol, ipAddress, port, user, password = "";
			// Get Policy Framework Service Endpoint (REST and WSS)
			paramList = this.lsr.getService(ServiceType.POLICY_FRAMEWORK);
			protocol = paramList.get("protocol");
			ipAddress = paramList.get("ip_address");
			port = paramList.get("out_port");
			PFApi pfClient = new PFApi (sliceId, protocol, ipAddress, port);
			
			// Get Data Lake Service Endpoint
			paramList = this.lsr.getService(ServiceType.DATA_LAKE);
			protocol = paramList.get("protocol");
			ipAddress = paramList.get("ip_address");
			port = paramList.get("out_port");
			DataLakeApi dlClient = new DataLakeApi(protocol, ipAddress, port);
			
			
			// Get SS-O Service Endpoint
			paramList = this.lsr.getService(ServiceType.SS_O);
			protocol = paramList.get("protocol");
			ipAddress = paramList.get("ip_address");
			port = paramList.get("out_port");
			user = paramList.get("user");
			password = paramList.get("password");
			ServiceOrchestratorDriver soDriver = new ServiceOrchestratorDriver(sliceId, protocol, ipAddress, port, user, password);
			
			//Get Kafka Bus Service Endpoint
			paramList = this.lsr.getService(ServiceType.KAFKA_BUS);
			ipAddress = paramList.get("ip_address");
			port = paramList.get("out_port");
			
			this.qoeOpt = new Optimizer(this, pfClient, dlClient, soDriver, ipAddress, port);
		}
		else if (registry.equals("Local"))
		{
			pfProto = env.getProperty("pfProto");
			pfIp = env.getProperty("pfIp");
			pfPort = env.getProperty("pfPort");
			dlIp = env.getProperty("datalakeIp");
			dlPort = env.getProperty("datalakePort");
			kfkIp = env.getProperty("kafkaIp");
			kfkPort = env.getProperty("kafkaPort");
			/*ksProto = env.getProperty("keystone.proto");
			ksIp = env.getProperty("keystone.ip");
			ksPort = env.getProperty("keystone.port");
			ksVersion = env.getProperty("keystone.version");
			tenantName = env.getProperty("tenant.name");
			domainName = env.getProperty("domain.name");
			userName = env.getProperty("usr.name");
			userPwd = env.getProperty("usr.pwd");
			
			this.qoeOpt = new Optimizer (this, pfProto, pfIp, pfPort, dlIp, dlPort, kfkIp, kfkPort, ksProto, ksIp, ksPort, ksVersion, tenantName, domainName, userName, userPwd);
			*/
			
			ssoProto = env.getProperty("sso.proto");
			ssoIp = env.getProperty("sso.ip");
			ssoPort = env.getProperty("sso.port");
			ssoUser = env.getProperty("sso.user");
			ssoPwd = env.getProperty("sso.pwd");
			this.qoeOpt = new Optimizer (this, pfProto, pfIp, pfPort, dlIp, dlPort, kfkIp, kfkPort, ssoProto, ssoIp, ssoPort, ssoUser, ssoPwd);
		}
		
		this.qoeOpt.setServerIp(serverIp);
		this.qoeOpt.setServerPort(serverPort);
		
		this.sliceId = UUID.fromString(sliceId);
		
		this.qoeConf.setRegistryIp(cpsrIp);
		this.qoeConf.setRegistryPort(cpsrPort);
		this.qoeConf.setSliceId(sliceId);
		this.qoeConf.setSliceOwner("vertical");
		
		this.registerQoECPS();
	}
	
	private void registerQoECPS ()
	{
		// Create CPS Services
		//this.cpsr.createCPSService("getQoE", "v1", "http", this.sliceId, null, this.qoeOpt.getServerIp(), "tcp", this.qoeOpt.getServerPort(), "/qoe", null, null, null, 0, 0);
		// Set QoE
		this.cpsr.createCPSService("qoe", "", "http", this.sliceId, null, this.qoeOpt.getServerIp(), "tcp", this.qoeOpt.getServerPort(), "/slicenet/qoe-optimizer/v1", null, null, null, 0, 0);
		// Get QoE
		//this.cpsr.createCPSService(null, null, "http", this.sliceId, null, this.qoeOpt.getServerIp(), "tcp", this.qoeOpt.getServerPort(), "/slicenet/qoe/v1", null, null, null, 0, 0);
		
		// Create CPS Profile
		this.cpsr.createCPSProfile(this.qoeOpt.getIdentifier(), this.sliceId);
		
		this.cpsr.registerOptimizer();
	}
	
	public void unregisterQoECPS ()
	{
		this.cpsr.unregisterOptimizer();
	}
	
	// Optimization
	public void sendQoEFeedback (String QoEFeedback)
	{
		//Event event = new Event(EventType.VerticalQoE, new ValueString (QoEFeedback), "QoE Input from vertical");
		this.qoeOpt.generateEvent(EventType.VerticalQoE, new ValueString (QoEFeedback), "QoE Input from vertical");
	}

	public QoEConf getQoeConf() {
		return qoeConf;
	}

	public void setQoeConf(QoEConf qoeConf) {
		this.qoeConf = qoeConf;
	}

	public QoEObject getQoeObj() {
		return qoeObj;
	}

	public void setQoeObj(QoEObject qoeObj) {
		this.qoeObj = qoeObj;
	}

	public Optimizer getQoeOpt() {
		return qoeOpt;
	}

	public void setQoeOpt(Optimizer qoeOpt) {
		this.qoeOpt = qoeOpt;
	}

	public CPSR getCpsr() {
		return cpsr;
	}

	public void setCpsr(CPSR cpsr) {
		this.cpsr = cpsr;
	}
	
}
