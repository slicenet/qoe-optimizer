package edu.upc.gco.slcnt.qoe.optimizer.rest.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import edu.upc.gco.slcnt.qoe.optimizer.rest.QoE;
import edu.upc.gco.slcnt.qoe.optimizer.rest.model.QoEConf;
import io.swagger.annotations.ApiParam;

import java.util.Optional;

import javax.validation.Valid;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2018-09-04T18:08:31.775+02:00[Europe/Madrid]")

@Controller
@RequestMapping("${openapi.qoeOptimizerV1.base-path:/slicenet/qoe-optimizer/v1}")
public class ConfApiController implements ConfApi {

	//QoEConf qoeConf = new QoEConf();
	private final NativeWebRequest request;
	@Autowired
	private ApplicationContext appContext;

	@org.springframework.beans.factory.annotation.Autowired
	public ConfApiController(NativeWebRequest request) {
		this.request = request;
	}

	@Override
	public Optional<NativeWebRequest> getRequest() {
		return Optional.ofNullable(request);
	}
	
	@Override
	public ResponseEntity<Void> confPlugin(@ApiParam(value = ""  )  @Valid @RequestBody QoEConf qoEConf) {
		
		QoE qoe = appContext.getBean(QoE.class);
		qoe.getQoeConf().setSliceId(qoEConf.getSliceId());
		qoe.getQoeConf().setSliceOwner(qoEConf.getSliceOwner());
		qoe.getQoeConf().setRegistryIp(qoEConf.getRegistryIp());
		qoe.getQoeConf().setRegistryPort(qoEConf.getRegistryPort());
		qoe.initQoE(qoEConf.getSliceId(), qoEConf.getRegistryIp(), qoEConf.getRegistryPort());
		
		return new ResponseEntity<>(HttpStatus.OK);
		//return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}
}
