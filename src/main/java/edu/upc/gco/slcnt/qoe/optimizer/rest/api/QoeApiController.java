package edu.upc.gco.slcnt.qoe.optimizer.rest.api;

import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import edu.upc.gco.slcnt.qoe.optimizer.rest.QoE;
import edu.upc.gco.slcnt.qoe.optimizer.rest.model.QoEObject;
import io.swagger.annotations.ApiParam;

import java.util.Optional;

import javax.validation.Valid;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2018-07-24T10:45:26.089+02:00[Europe/Madrid]")

@Controller
@RequestMapping("${openapi.qoeOptimizerV1.base-path:/slicenet/qoe-optimizer/v1}")
public class QoeApiController implements QoeApi {

    @Autowired
	private ApplicationContext appContext;
    private final NativeWebRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public QoeApiController(NativeWebRequest request) {
        this.request = request;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<String> getQoE(@ApiParam(value = "Id of the QoE Instance",required=true) @PathVariable("id") String id) {
        QoE qoe = appContext.getBean(QoE.class);
    	String str = qoe.getQoeObj().getValue();
    	
        return new ResponseEntity<>(str, HttpStatus.OK);
        //return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<Void> setQoE(@ApiParam(value = ""  )  @Valid @RequestBody QoEObject qoEObject) {

        QoE qoe = appContext.getBean(QoE.class);
        qoe.getQoeObj().setId(qoEObject.getId());
        qoe.getQoeObj().setValue(qoEObject.getValue());
        
        qoe.sendQoEFeedback(qoEObject.getValue());
        
        return new ResponseEntity<>(HttpStatus.OK);
        //return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

}
