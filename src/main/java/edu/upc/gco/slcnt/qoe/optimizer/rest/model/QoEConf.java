package edu.upc.gco.slcnt.qoe.optimizer.rest.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * QoEConf
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2018-09-04T18:08:31.775+02:00[Europe/Madrid]")

public class QoEConf {
	@JsonProperty("slice_id")
	private String sliceId = null;

	@JsonProperty("slice_owner")
	private String sliceOwner = null;

	@JsonProperty("register_ip")
	private String registryIp = null;

	@JsonProperty("register_port")
	private String registryPort = null;

	public QoEConf sliceId(String sliceId) {
		this.sliceId = sliceId;
		return this;
	}
	
	/**
	 * Get sliceId
	 * @return sliceId
	 **/
	@ApiModelProperty(value = "")

	public String getSliceId() {
		return sliceId;
	}

	public void setSliceId(String sliceId) {
		this.sliceId = sliceId;
	}

	public QoEConf sliceOwner(String sliceOwner) {
		this.sliceOwner = sliceOwner;
		return this;
	}

	/**
	 * Get sliceOwner
	 * @return sliceOwner
	**/
	@ApiModelProperty(value = "")

	public String getSliceOwner() {
		return sliceOwner;
	}

	public void setSliceOwner(String sliceOwner) {
		this.sliceOwner = sliceOwner;
	}

	public QoEConf registryIp(String registryIp) {
		this.registryIp = registryIp;
		return this;
	}

	/**
	 * Get registryIp
	 * @return registryIp
	**/
	@ApiModelProperty(value = "")

	public String getRegistryIp() {
		return registryIp;
	}

	public void setRegistryIp(String registryIp) {
		this.registryIp = registryIp;
	}

	public QoEConf registryPort(String registryPort) {
		this.registryPort = registryPort;
		return this;
	}

	/**
	 * Get registryPort
	 * @return registryPort
	**/
	@ApiModelProperty(value = "")

	public String getRegistryPort() {
		return registryPort;
	}

	public void setRegistryPort(String registryPort) {
		this.registryPort = registryPort;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
	    if (o == null || getClass() != o.getClass()) {
	      return false;
	    }
	    QoEConf qoEConf = (QoEConf) o;
	    return Objects.equals(this.sliceId, qoEConf.sliceId) &&
	    		Objects.equals(this.sliceOwner, qoEConf.sliceOwner) &&
	    		Objects.equals(this.registryIp, qoEConf.registryIp) &&
	    		Objects.equals(this.registryPort, qoEConf.registryPort);
	}

	@Override
	public int hashCode() {
		return Objects.hash(sliceId, sliceOwner, registryIp, registryPort);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class QoEConf {\n");
	    
		sb.append("    slice_id: ").append(toIndentedString(sliceId)).append("\n");
		sb.append("    slice_owner: ").append(toIndentedString(sliceOwner)).append("\n");
		sb.append("    register_ip: ").append(toIndentedString(registryIp)).append("\n");
		sb.append("    register_port: ").append(toIndentedString(registryPort)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	*/
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
